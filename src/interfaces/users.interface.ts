export interface User {
  id: string;
  email: string;
  fullName: string;
  isEmailVerified: boolean;
  roleId: string;
}
