const roles = [
  {
    role: 'SuperAdmin',
    permission: {
      'User Management': {
        read: true,
        write: true,
        delete: true,
      },
      'Admin Management': {
        read: true,
        write: true,
        delete: true,
      },
      'Flavors & Feedback': {
        read: false,
        write: false,
        delete: false,
      },
    },
  },
  {
    role: 'Restaurant',
    permission: {
      'User Management': {
        read: false,
        write: false,
        delete: false,
      },
      'Admin Management': {
        read: false,
        write: false,
        delete: false,
      },
      'Flavors & Feedback': {
        read: true,
        write: true,
        delete: true,
      },
    },
  },
];

export default roles;
