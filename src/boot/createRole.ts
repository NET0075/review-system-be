import DB from '@/databases';
import { logger } from '@utils/logger';
import roles from './data/role';

class CreateRoles {
  public role = DB.Role;
  public init = async () => {
    try {
      const res = await this.role.count();
      if (res !== 0) return;
      await this.role.bulkCreate(roles);
    } catch (error) {
      logger.error(error);
    }
  };
}
export default CreateRoles;
