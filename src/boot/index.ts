import CreateRoles from './createRole';
import CreateUser from './createUser';

class BootFiles {
  public createRole = new CreateRoles();
  public createUser = new CreateUser();

  public async init() {
    await this.createRole.init();
    await this.createUser.init();
  }
}
export default BootFiles;
