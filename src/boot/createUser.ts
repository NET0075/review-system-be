import DB from '@/databases';
import UserData from './data/user';

class CreateUser {
  public users = DB.User;
  public role = DB.Role;

  public init = async () => {
    try {
      const existingUserCount = await this.users.count();
      if (existingUserCount !== 0) return;

      for (const user of UserData) {
        const role = await this.role.findOne({ where: { role: user.role } });
        const users = await this.users.create({
          fullName: user.fullName,
          email: user.email,
          password: user.password,
          isEmailVerified: user.isEmailVerified,
          isApproved: user.isApproved,
          roleId: role.id,
        });
      }

      // Additional code to create a specific user from UserData if it doesn't exist
      const userInstance = await this.users.findOne({
        where: { email: UserData[0].email },
      });

      if (!userInstance) {
        await this.users.create({
          ...UserData[0],
        });
      }
    } catch (error) {
      console.error('Error occurred during user creation:', error);
    }
  };
}

export default CreateUser;
