import { TWILIO_ACCOUNT_SID, TWILIO_AUTH_TOKEN, TWILIO_PHONE_NUMBER } from '.';
import twilio from 'twilio';
import { logger } from '@/utils/logger';

const client = twilio(TWILIO_ACCOUNT_SID, TWILIO_AUTH_TOKEN);

//send verification code token
export const sendVerification = async (number, code) => {
  try {
    return await client.messages.create({
      body: `Thank you for Signing up on Food Genie. Please Use the Verification OTP to Verify your Login: ${code}`,
      from: TWILIO_PHONE_NUMBER,
      to: `${number}`,
    });
  } catch (error) {
    logger.error(error);
  }
};
