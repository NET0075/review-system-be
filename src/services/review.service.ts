import { ReviewDTO } from '../dtos/review.dto';
import { HttpException } from '@/exceptions/HttpException';
import { Review } from '@/interfaces/review.interface';
import DB from '@databases';
import moment from 'moment';
import { Op } from 'sequelize';

class ReviewService {
  public user = DB.User;
  public course = DB.Course;
  public product = DB.Product;
  public review = DB.Review;
  private readonly reviewAvailableTime = 60 * 60 * 1000;

  public isAdmin(userData): boolean {
    return userData.role === 'SuperAdmin' || userData.role === 'Admin';
  }

  public async createReview({ reviewDetail, customer }): Promise<ReviewDTO[]> {
    const restaurantData = await this.user.findOne({
      where: { id: reviewDetail.restaurantId },
    });

    if (!restaurantData) {
      throw new HttpException(404, 'User not found');
    }

    const lastReview = await this.review.findOne({
      where: {
        customerId: customer.id,
        userId: reviewDetail.restaurantId,
      },
      order: [['createdAt', 'DESC']],
    });

    if (lastReview) {
      // Restrict customer for one hours to add review on same Restaurant
      const currentTime = new Date().getTime();
      const lastReviewTime = lastReview.createdAt.getTime();
      const timeDiff = currentTime - lastReviewTime;

      if (timeDiff < this.reviewAvailableTime) {
        const remainingMinutes = Math.floor((this.reviewAvailableTime - timeDiff) / (60 * 1000));
        const remainingSeconds = Math.ceil(((this.reviewAvailableTime - timeDiff) % (60 * 1000)) / 1000);

        const formattedSeconds = remainingSeconds < 10 ? `0${remainingSeconds}` : `${remainingSeconds}`;
        throw new HttpException(400, `Please wait for ${remainingMinutes}:${formattedSeconds} minutes before submitting another review.`);
      }
    }
    const review = await this.review.create({
      ...reviewDetail,
      issueType: reviewDetail.issueType,
      userId: reviewDetail.restaurantId,
      customerId: customer.id,
      otherDescription: reviewDetail.otherDescription ? reviewDetail.otherDescription : null,
    });

    return review;
  }

  public async getReview({ queryObject, restaurantId }): Promise<{ totalCount: number; records: (Review | undefined)[] }> {
    const sortBy = queryObject.sortBy ? queryObject.sortBy : 'createdAt';
    const order = queryObject.order || 'DESC';
    // pagination
    const pageSize = queryObject.pageRecord ? queryObject.pageRecord : 10;
    const pageNo = queryObject.pageNo ? (queryObject.pageNo - 1) * pageSize : 0;
    // Search
    const [search, searchCondition] = queryObject.search ? [`%${queryObject.search}%`, DB.Sequelize.Op.iLike] : ['', DB.Sequelize.Op.ne];
    const reviewData = await this.review.findAndCountAll({
      where: DB.Sequelize.or({
        userId: restaurantId,
      }),
      include: [
        {
          model: this.user,
          where: {
            fullName: {
              [searchCondition]: search,
            },
          },
        },
      ],

      limit: pageSize,
      offset: pageNo,
      order: [[`${sortBy}`, `${order}`]],
    });

    return { totalCount: reviewData.count, records: reviewData.rows };
  }

  public async deleteReview(reviewId): Promise<{ count: number }> {
    const res: number = await this.review.destroy({
      where: {
        id: reviewId,
      },
      force: true,
    });
    return { count: res };
  }

  public async deleteReviewBySuperAdmin({ reviewId, user }): Promise<{ count: number }> {
    if (!this.isAdmin(user)) throw new HttpException(401, 'Unauthorized');

    const res: number = await this.review.destroy({
      where: {
        id: reviewId,
      },
    });
    return { count: res };
  }

  public async getAllReviewMonthWise(restaurant) {
    const months = [];
    for (let i = 1; i <= 12; i++) {
      const recordsPerMonth = await this.review.findAndCountAll({
        where: {
          userId: restaurant.id,
          createdAt: {
            [Op.gte]: moment()
              .month(i - 1)
              .startOf('month')
              .year(moment().year())
              .toDate(),

            [Op.lt]: moment()
              .month(i - 1)
              .endOf('month')
              .year(moment().year())
              .toDate(),
          },
        },
      });
      months.push(recordsPerMonth);
    }
    return months;
  }
}
export default ReviewService;
