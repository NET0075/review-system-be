import { HttpException } from '@/exceptions/HttpException';
import DB from '@databases';
import { Op } from 'sequelize';

class PaymentGatewayService {
  public user = DB.User;
  public paymentGateway = DB.PaymentGateway;

  public async createPaymentGateway({ gatewayData, file, user }) {
    const thumbnail = file;
    const thumbnailPath = `/media/${thumbnail.path.split('/').splice(-2).join('/')}`;

    // const thumbnailPath = `/media/${file.path.split('/').splice(-2).join('/')}`;

    const findGateway = await this.paymentGateway.findOne({
      where: DB.Sequelize.and({
        title: {
          [Op.iLike]: `%${gatewayData.title}%`,
        },
      }),
    });
    if (findGateway) throw new HttpException(409, 'Gateway already exist');

    const createGateway = await this.paymentGateway.create({
      ...gatewayData,
      logo: thumbnailPath,
    });
    return createGateway;
  }

  public async getAllGateway({ queryObject }): Promise<{ totalCount: number; records: undefined }> {
    const sortBy = queryObject.sortBy ? queryObject.sortBy : 'createdAt';
    const order = queryObject.order || 'DESC';
    // pagination
    const pageSize = queryObject.pageRecord ? queryObject.pageRecord : 10;
    const pageNo = queryObject.pageNo ? (queryObject.pageNo - 1) * pageSize : 0;
    // Search
    const [search, searchCondition] = queryObject.search ? [`%${queryObject.search}%`, DB.Sequelize.Op.iLike] : ['', DB.Sequelize.Op.ne];
    const findGateway = await this.paymentGateway.findAndCountAll({ limit: pageSize, offset: pageNo, order: [[`${sortBy}`, `${order}`]] });
    return { totalCount: findGateway.count, records: findGateway.rows };
  }

  public async getAllGateways(): Promise<{ totalCount: number; records: undefined }> {
    const findGateway = await this.paymentGateway.findAndCountAll({
      order: [[`createdAt`, 'DESC']],
    });
    return { totalCount: findGateway.count, records: findGateway.rows };
  }

  public async getGatewayById({ paymentGatewayId }) {
    const findGateway = await this.paymentGateway.findOne({
      where: { id: paymentGatewayId },
    });
    if (!findGateway) throw new HttpException(404, 'No data found');
    return findGateway;
  }

  public async updateGateway({ gatewayData, paymentGatewayId, user, file }) {
    const findGateway = await this.paymentGateway.findOne({
      where: { id: paymentGatewayId },
    });
    if (!findGateway) throw new HttpException(404, 'No data found');

    const findGatewayWithTitle = await this.paymentGateway.findOne({
      where: DB.Sequelize.and({
        title: {
          [Op.iLike]: `%${gatewayData.title}%`,
        },
        id: {
          [Op.ne]: paymentGatewayId,
        },
      }),
    });
    if (findGatewayWithTitle) throw new HttpException(409, 'Gateway title already exist');

    let updateGateway;

    if (file) {
      const thumbnail = file;
      const thumbnailPath = `/media/${thumbnail.path.split('/').splice(-2).join('/')}`;

      // const thumbnailPath = `/media/${file.path.split('/').splice(-2).join('/')}`;

      updateGateway = await this.paymentGateway.update(
        {
          ...gatewayData,
          logo: thumbnailPath,
        },
        { where: { id: paymentGatewayId }, returning: true },
      );
    } else {
      updateGateway = await this.paymentGateway.update(
        {
          ...gatewayData,
        },
        { where: { id: paymentGatewayId }, returning: true },
      );
    }
    return updateGateway[1];
  }

  public async deleteGateway({ paymentGatewayId, user }) {
    const findGateway = await this.paymentGateway.findOne({
      where: { id: paymentGatewayId },
    });
    if (!findGateway) throw new HttpException(404, 'No data found');
    await findGateway.destroy();
    return findGateway;
  }
}
export default PaymentGatewayService;
