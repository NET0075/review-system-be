import { hash } from 'bcrypt';
import DB from '@databases';
import { HttpException } from '@exceptions/HttpException';
import { User } from '@interfaces/users.interface';
import { isEmpty } from '@utils/util';
import { Op } from 'sequelize';
import moment from 'moment';
import { RegisterUserDto } from '@/dtos/users.dto';

class RestaurantService {
  public users = DB.User;
  public profile = DB.RestaurantProfile;
  public review = DB.Review;
  public socialLinks = DB.SocialLinks;
  public gateways = DB.Gateway;
  public paymentGateways = DB.PaymentGateway;
  public platform = DB.ReviewPlatform;
  public menuCategory = DB.Categories;
  public menu = DB.Menu;
  public role = DB.Role;
  public customer = DB.Customer;

  public async findAllRestaurant(queryObject) {
    const sortBy = queryObject.sortBy ? queryObject.sortBy : 'createdAt';
    const order = queryObject.order || 'DESC';
    // pagination
    const pageSize = queryObject.pageRecord ? queryObject.pageRecord : 10;
    const pageNo = queryObject.pageNo ? (queryObject.pageNo - 1) * pageSize : 0;

    // Search
    const [search, searchCondition] = queryObject.search ? [`%${queryObject.search}%`, DB.Sequelize.Op.iLike] : ['', DB.Sequelize.Op.ne];

    const status = queryObject?.isApproved || null;

    const findRestaurant = await this.role.findOne({
      where: {
        role: 'Restaurant',
      },
    });

    const allUser = await this.users.findAndCountAll({
      where: DB.Sequelize.and(
        {
          roleId: findRestaurant.id,
          [Op.or]: [{ fullName: { [searchCondition]: search } }, { email: { [searchCondition]: search } }],
        },
        status !== null
          ? {
              isApproved: status,
            }
          : null,
      ),
      include: [{ model: this.profile }],
      distinct: true,
      limit: pageSize,
      offset: pageNo,
      order: [[`${sortBy}`, `${order}`]],
    });
    const restaurantCount = await this.users.findAll({
      where: {
        roleId: {
          [Op.eq]: findRestaurant.id,
        },
      },
    });
    const activeCount = await restaurantCount.filter(count => count.isApproved === 'Active').length;
    const pendingCount = await restaurantCount.filter(count => count.isApproved === 'Pending').length;
    const inActiveCount = await restaurantCount.filter(count => count.isApproved === 'InActive').length;

    return { totalCount: allUser.count, records: allUser.rows, activeCount, pendingCount, inActiveCount };
  }

  public async getRestaurantById(restaurantId: string) {
    if (isEmpty(restaurantId)) throw new HttpException(400, 'RestaurantId is empty');

    const findTempUser = await this.users.findOne({
      where: {
        id: restaurantId,
      },
      include: [
        {
          model: this.profile,
          include: [{ model: this.gateways, include: [{ model: this.paymentGateways }] }],
        },
        { model: this.role },
      ],
    });
    if (!findTempUser) throw new HttpException(409, "Restaurant doesn't exist");

    if (findTempUser.role == 'SuperAdmin') {
      return findTempUser;
    } else if (findTempUser.role == 'Restaurant' && !findTempUser.RestaurantProfile.isProfileUpdated) {
      return findTempUser;
    } else if (findTempUser.RestaurantProfile && findTempUser.RestaurantProfile.isCustomForm) {
      return findTempUser;
    } else {
      const findUser = await this.users.findOne({
        where: {
          id: restaurantId,
        },
        include: [
          {
            model: this.profile,
            include: [
              { model: this.socialLinks, include: [{ model: this.platform }] },
              { model: this.gateways, include: [{ model: this.paymentGateways }] },
            ],
          },
          { model: this.role },
        ],
      });

      return findUser;
    }
  }

  public async findRestaurantById(restaurantId: string) {
    if (isEmpty(restaurantId)) throw new HttpException(400, 'RestaurantId is empty');

    const findTempUser = await this.users.findOne({
      where: {
        id: restaurantId,
      },
      include: [
        {
          model: this.profile,
          include: [{ model: this.gateways, include: [{ model: this.paymentGateways }] }],
        },
        { model: this.role },
      ],
    });
    if (!findTempUser) throw new HttpException(409, "Restaurant doesn't exist");

    if (findTempUser.role == 'SuperAdmin') {
      return findTempUser;
    } else if (findTempUser.role == 'Restaurant' && !findTempUser.RestaurantProfile.isProfileUpdated) {
      return findTempUser;
    } else if (findTempUser.RestaurantProfile && findTempUser.RestaurantProfile.isCustomForm) {
      return findTempUser;
    } else {
      const findUser = await this.users.findOne({
        where: {
          id: restaurantId,
        },
        include: [
          {
            model: this.profile,
            include: [
              { model: this.socialLinks, include: [{ model: this.platform }] },
              { model: this.gateways, include: [{ model: this.paymentGateways }] },
            ],
          },
          { model: this.role },
        ],
      });

      return findUser;
    }
  }

  public async createRestaurant({ restaurantData, file }) {
    if (isEmpty(restaurantData)) throw new HttpException(400, 'RestaurantData is empty');

    const findUser: User = await this.users.findOne({ where: { email: restaurantData.email } });
    if (findUser) throw new HttpException(409, `This email ${restaurantData.email} already exists`);

    const findRole = await this.role.findOne({ where: { role: 'Restaurant' } });
    if (!findRole) throw new HttpException(404, 'No role found');

    const thumbnailPath = `/media/${file.path.split('/').splice(-2).join('/')}`;

    const createUserData: User = await this.users.create({
      fullName: restaurantData.fullName,
      email: restaurantData.email,
      password: restaurantData.password,
      isApproved: 'Active',
      roleId: findRole.id,
    });
    const createUserProfile = await this.profile.create({
      address1: restaurantData.address1,
      address2: restaurantData.address2,
      city: restaurantData.city,
      state: restaurantData.state,
      phoneNo: restaurantData.phoneNo,
      country: restaurantData.country,
      pinCode: restaurantData.pinCode,
      logo: thumbnailPath,
      qrCode: restaurantData.qrCode,
      isCustomForm: restaurantData.isCustomForm,
      isProfileUpdated: true,
      latitude: restaurantData.latitude,
      longitude: restaurantData.longitude,
      userId: createUserData.id,
    });

    let finalLinks = [];
    if (restaurantData.reviewPlatforms) {
      const platformLink = JSON.parse(restaurantData.reviewPlatforms);
      if (platformLink?.length > 0) {
        const findLinks = await this.socialLinks.findAll({ where: { restaurantProfileId: createUserProfile.id } });
        if (findLinks.length) {
          findLinks.map(async links => {
            await links.destroy({ force: true });
          });
        }
        finalLinks = await Promise.all(
          platformLink.map(async url => {
            const newLink = await this.socialLinks.create({
              socialLinks: url?.link,
              restaurantProfileId: createUserProfile.id,
              platformId: url?.platform,
              isSelected: url.isSelected,
            });
            return newLink;
          }),
        );
      }
    } else {
      const findLinks = await this.socialLinks.findAll({ where: { restaurantProfileId: createUserProfile.id } });
      if (findLinks.length) {
        findLinks.map(async links => {
          await links.destroy({ force: true });
        });
      }
    }

    let newGateway = null;
    if (restaurantData.paymentGatewayInfo) {
      const gatewayInfo = JSON.parse(restaurantData.paymentGatewayInfo);

      const findGateway = await this.gateways.findOne({ where: { restaurantProfileId: createUserProfile.id } });
      if (findGateway) {
        await findGateway.destroy({ force: true });
      }

      newGateway = await this.gateways.create({
        fields: JSON.stringify(gatewayInfo?.fields),
        restaurantProfileId: createUserProfile.id,
        paymentGatewayId: gatewayInfo?.paymentGateway,
      });
    }

    const restaurantProfileData = {
      createUserData,
      createUserProfile,
      finalLinks,
      newGateway,
    };
    return restaurantProfileData;
  }

  public async updateRestaurant(restaurantId: string, restaurantData: RegisterUserDto): Promise<User> {
    if (isEmpty(restaurantData)) throw new HttpException(400, 'RestaurantData is empty');

    const findUser: User = await this.users.findByPk(restaurantId);
    if (!findUser) throw new HttpException(409, "Restaurant doesn't exist");

    await this.users.update({ ...restaurantData, password: restaurantData.password }, { where: { id: restaurantId } });

    const updateUser: User = await this.users.findByPk(restaurantId);
    return updateUser;
  }

  public async deleteRestaurant(restaurantId: string): Promise<User> {
    if (isEmpty(restaurantId)) throw new HttpException(400, "Restaurant doesn't existId");

    const findUser = await this.users.findOne({ where: { id: restaurantId } });
    if (!findUser) throw new HttpException(409, "Restaurant doesn't exist");
    if (findUser.isApproved === 'Active') {
      throw new HttpException(409, 'Error: Deletion of Active Restaurant is not permitted. \uD83D\uDE41');
    } else {
      await findUser.destroy({
        include: [
          {
            model: this.profile,
            include: [{ model: this.socialLinks }],
          },
          { model: this.review },
          { model: this.menuCategory, include: [{ model: this.menu }] },
        ],
        force: true,
      });

      return findUser;
    }
  }
}

export default RestaurantService;
