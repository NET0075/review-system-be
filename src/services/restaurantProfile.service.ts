import DB from '@databases';
import { HttpException } from '@exceptions/HttpException';
import calcCoordsDistance, { Coordinates } from '@/utils/latlogcalculation';
import { getCountry, getCurrency } from 'country-currency-map';

class RestaurantProfileService {
  public users = DB.User;
  public passwordToken = DB.ResetPasswordToken;
  public profile = DB.RestaurantProfile;
  public socialLink = DB.SocialLinks;
  public gateways = DB.Gateway;
  public review = DB.Review;
  public reviewPlatform = DB.ReviewPlatform;
  public menuCategories = DB.Categories;
  public menu = DB.Menu;

  public async createRestaurantProfile({ user, profileData, file }) {
    const findProfile = await this.profile.findOne({ where: { userId: user.id, deletedAt: null } });
    if (findProfile) {
      throw new HttpException(409, 'Profile already exist');
    }

    const thumbnail = file;
    const thumbnailPath = `/media/${thumbnail.path.split('/').splice(-2).join('/')}`;

    // const thumbnailPath = `/media/${file.path.split('/').splice(-2).join('/')}`;

    const createprofile = await this.profile.create({
      ...profileData,
      id: user.id,
      userId: user.id,
      qrCode: profileData?.qrCode,
      logo: thumbnailPath,
    });
    let finalLinks = [];

    if (profileData?.socialLinks?.length > 0) {
      finalLinks = await Promise.all(
        profileData.socialLinks.map(async url => {
          const newLink = await this.socialLink.create({
            socialLinks: url,
            restaurantProfileId: createprofile.id,
            platformId: profileData.platformId,
          });
          return newLink;
        }),
      );
    }

    return { createprofile, finalLinks };
  }
  public async getRestaurantProfile({ user }) {
    const findRestaurantProfile = await this.profile.findOne({
      where: {
        userId: user.id,
      },
    });
    if (!findRestaurantProfile) throw new HttpException(404, 'No data found');
    return findRestaurantProfile;
  }

  public async updateRestaurantProfileById({ restaurantId, profileData, file }) {
    const findRestaurantProfile = await this.profile.findOne({
      where: {
        userId: restaurantId,
      },
    });
    if (!findRestaurantProfile) throw new HttpException(404, 'No data found');

    let finalLinks = [];
    if (profileData.reviewPlatforms) {
      const platformLink = JSON.parse(profileData.reviewPlatforms);
      if (platformLink?.length > 0) {
        const findLinks = await this.socialLink.findAll({ where: { restaurantProfileId: findRestaurantProfile.id } });
        if (findLinks.length) {
          findLinks.map(async links => {
            await links.destroy({ force: true });
          });
        }
        finalLinks = await Promise.all(
          platformLink.map(async url => {
            const newLink = await this.socialLink.create({
              socialLinks: url?.link,
              restaurantProfileId: findRestaurantProfile.id,
              platformId: url?.platform,
              isSelected: url.isSelected,
            });
            return newLink;
          }),
        );
      }
    } else {
      const findLinks = await this.socialLink.findAll({ where: { restaurantProfileId: findRestaurantProfile.id } });
      if (findLinks.length) {
        findLinks.map(async links => {
          await links.destroy({ force: true });
        });
      }
    }

    let newGateway = null;
    if (profileData.paymentGatewayInfo) {
      const gatewayInfo = JSON.parse(profileData.paymentGatewayInfo);

      const findGateway = await this.gateways.findOne({ where: { restaurantProfileId: findRestaurantProfile.id } });
      if (findGateway) {
        await findGateway.destroy({ force: true });
      }

      console.log(gatewayInfo);
      newGateway = await this.gateways.create({
        fields: JSON.stringify(gatewayInfo?.fields),
        restaurantProfileId: findRestaurantProfile.id,
        paymentGatewayId: gatewayInfo?.paymentGateway,
      });
    }

    const findAllCategory = await this.menuCategories.findAll({
      where: { userId: restaurantId },
    });
    const updateUser = await this.users.update(
      {
        fullName: profileData.fullName,
      },
      {
        where: {
          id: restaurantId,
        },
        returning: true,
      },
    );
    if (file) {
      const thumbnail = file;
      const thumbnailPath = `/media/${thumbnail.path.split('/').splice(-2).join('/')}`;

      // const thumbnailPath = `/media/${file.path.split('/').splice(-2).join('/')}`;

      const updateRestaurantProfile = await this.profile.update(
        {
          ...profileData,
          address1: profileData.address1,
          address2: profileData.address2,
          city: profileData.city,
          state: profileData.state,
          pinCode: profileData.pinCode,
          phoneNo: profileData.phoneNo,
          country: profileData.country,
          qrCode: profileData.qrCode,
          logo: thumbnailPath,
          isCustomForm: JSON.parse(profileData.isCustomForm),
          isProfileUpdated: true,
          latitude: profileData.latitude,
          longitude: profileData.longitude,
        },
        {
          where: {
            id: findRestaurantProfile.id,
          },
          returning: true,
        },
      );
      let updateProfile = updateRestaurantProfile[1][0];

      let country = getCountry(updateProfile.country);
      let currency = getCurrency(country.currency);
      let cleanSymbolFormat = currency.symbolFormat.replace(/\{#\}/g, '');

      let updatedMenus = [];

      await Promise.all(
        findAllCategory.map(async menus => {
          const findMenu = await this.menu.findAll({
            where: {
              categoryId: menus.id,
            },
          });

          const updatedMenu = await Promise.all(
            findMenu.map(async menu => {
              const updated = await this.menu.update(
                {
                  currencySymbol: cleanSymbolFormat,
                },
                {
                  where: { categoryId: menus.id },
                },
              );
              return updated;
            }),
          );

          updatedMenus.push(updatedMenu);
        }),
      );

      return { updateRestaurantProfile: updateRestaurantProfile[1], finalLinks };
    }

    const updateRestaurantProfile = await this.profile.update(
      {
        ...profileData,
        address1: profileData.address1,
        address2: profileData.address2,
        city: profileData.city,
        state: profileData.state,
        pinCode: profileData.pinCode,
        phoneNo: profileData.phoneNo,
        country: profileData.country,
        qrCode: profileData.qrCode,
        logo: findRestaurantProfile.logo,
        isCustomForm: JSON.parse(profileData.isCustomForm),
        isProfileUpdated: true,
        latitude: profileData.latitude,
        longitude: profileData.longitude,
      },
      {
        where: {
          id: findRestaurantProfile.id,
        },
        returning: true,
      },
    );

    let updateProfile = updateRestaurantProfile[1][0];

    let country = getCountry(updateProfile.country);
    let currency = getCurrency(country.currency);
    let cleanSymbolFormat = currency.symbolFormat.replace(/\{#\}/g, '');

    let updatedMenus = [];

    await Promise.all(
      findAllCategory.map(async menus => {
        const findMenu = await this.menu.findAll({
          where: {
            categoryId: menus.id,
          },
        });

        const updatedMenu = await Promise.all(
          findMenu.map(async (menu: any) => {
            const updated = await this.menu.update(
              {
                currencySymbol: cleanSymbolFormat,
              },
              {
                where: { categoryId: menus.id },
              },
            );

            return updated;
          }),
        );

        updatedMenus.push(updatedMenu);
      }),
    );
    return { updateRestaurantProfile: updateRestaurantProfile[1], finalLinks };
  }

  public async updateRestaurantProfile({ user, profileData, file }) {
    const findRestaurantProfile = await this.profile.findOne({
      where: {
        userId: user.id,
      },
    });
    if (!findRestaurantProfile) throw new HttpException(404, 'No data found');

    let finalLinks = [];
    if (profileData.reviewPlatforms) {
      const platformLink = JSON.parse(profileData.reviewPlatforms);
      if (platformLink?.length > 0) {
        const findLinks = await this.socialLink.findAll({ where: { restaurantProfileId: findRestaurantProfile.id } });
        if (findLinks.length) {
          findLinks.map(async links => {
            await links.destroy({ force: true });
          });
        }
        finalLinks = await Promise.all(
          platformLink.map(async url => {
            const newLink = await this.socialLink.create({
              socialLinks: url?.link,
              restaurantProfileId: findRestaurantProfile.id,
              platformId: url?.platform,
              isSelected: url.isSelected,
            });
            return newLink;
          }),
        );
      }
    } else {
      const findLinks = await this.socialLink.findAll({ where: { restaurantProfileId: findRestaurantProfile.id } });
      if (findLinks.length) {
        findLinks.map(async links => {
          await links.destroy({ force: true });
        });
      }
    }

    let newGateway = null;
    if (profileData.paymentGatewayInfo) {
      const gatewayInfo = JSON.parse(profileData.paymentGatewayInfo);

      const findGateway = await this.gateways.findOne({ where: { restaurantProfileId: findRestaurantProfile.id } });
      if (findGateway) {
        await findGateway.destroy({ force: true });
      }

      console.log(gatewayInfo);
      newGateway = await this.gateways.create({
        fields: JSON.stringify(gatewayInfo?.fields),
        restaurantProfileId: findRestaurantProfile.id,
        paymentGatewayId: gatewayInfo?.paymentGateway,
      });
    }

    const findAllCategory = await this.menuCategories.findAll({
      where: { userId: user.id },
    });

    if (file) {
      const thumbnail = file;
      const thumbnailPath = `/media/${thumbnail.path.split('/').splice(-2).join('/')}`;

      // const thumbnailPath = `/media/${file.path.split('/').splice(-2).join('/')}`;

      const updateRestaurantProfile = await this.profile.update(
        {
          ...profileData,
          address1: profileData.address1,
          address2: profileData.address2,
          city: profileData.city,
          state: profileData.state,
          pinCode: profileData.pinCode,
          phoneNo: profileData.phoneNo,
          country: profileData.country,
          qrCode: profileData.qrCode,
          logo: thumbnailPath,
          isCustomForm: JSON.parse(profileData.isCustomForm),
          isProfileUpdated: true,
          latitude: profileData.latitude,
          longitude: profileData.longitude,
        },
        {
          where: {
            id: findRestaurantProfile.id,
          },
          returning: true,
        },
      );
      let updateProfile = updateRestaurantProfile[1][0];

      let country = getCountry(updateProfile.country);
      let currency = getCurrency(country.currency);
      let cleanSymbolFormat = currency.symbolFormat.replace(/\{#\}/g, '');

      let updatedMenus = [];

      await Promise.all(
        findAllCategory.map(async menus => {
          const findMenu = await this.menu.findAll({
            where: {
              categoryId: menus.id,
            },
          });

          const updatedMenu = await Promise.all(
            findMenu.map(async menu => {
              const updated = await this.menu.update(
                {
                  currencySymbol: cleanSymbolFormat,
                },
                {
                  where: { categoryId: menus.id },
                },
              );
              return updated;
            }),
          );

          updatedMenus.push(updatedMenu);
        }),
      );

      return { updateRestaurantProfile: updateRestaurantProfile[1], finalLinks };
    }

    const updateRestaurantProfile = await this.profile.update(
      {
        ...profileData,
        address1: profileData.address1,
        address2: profileData.address2,
        city: profileData.city,
        state: profileData.state,
        pinCode: profileData.pinCode,
        phoneNo: profileData.phoneNo,
        country: profileData.country,
        qrCode: profileData.qrCode,
        logo: findRestaurantProfile.logo,
        isCustomForm: JSON.parse(profileData.isCustomForm),
        isProfileUpdated: true,
        latitude: profileData.latitude,
        longitude: profileData.longitude,
      },
      {
        where: {
          id: findRestaurantProfile.id,
        },
        returning: true,
      },
    );

    let updateProfile = updateRestaurantProfile[1][0];

    let country = getCountry(updateProfile.country);
    let currency = getCurrency(country.currency);
    let cleanSymbolFormat = currency.symbolFormat.replace(/\{#\}/g, '');

    let updatedMenus = [];

    await Promise.all(
      findAllCategory.map(async menus => {
        const findMenu = await this.menu.findAll({
          where: {
            categoryId: menus.id,
          },
        });

        const updatedMenu = await Promise.all(
          findMenu.map(async (menu: any) => {
            const updated = await this.menu.update(
              {
                currencySymbol: cleanSymbolFormat,
              },
              {
                where: { categoryId: menus.id },
              },
            );

            return updated;
          }),
        );

        updatedMenus.push(updatedMenu);
      }),
    );
    return { updateRestaurantProfile: updateRestaurantProfile[1], finalLinks };
  }

  public async deleteRestaurantProfile({ user }) {
    const findRestaurantProfile = await this.profile.findOne({
      where: {
        userId: user.id,
      },
    });
    if (!findRestaurantProfile) throw new HttpException(404, 'No data found');

    await findRestaurantProfile.destroy({ include: [{ model: this.socialLink }] });
    return findRestaurantProfile;
  }

  public async deleteSocialLinks({ socialLinkId, user }) {
    const findRestaurant = await this.users.findOne({ where: { id: user.id } });
    if (!findRestaurant) {
      throw new HttpException(403, 'Forbidden Resources');
    } else {
      const findSocialLink = await this.socialLink.findOne({ where: { id: socialLinkId } });
      if (!findSocialLink) throw new HttpException(404, 'No data found');
      await findSocialLink.destroy();
      return findSocialLink;
    }
  }

  public async getRestaurantByQRCode({ qrCode, user, latitude, longitude }) {
    let findProfile = await this.profile.findOne({
      where: { qrCode },
      include: [
        { model: this.users, include: [{ model: this.review }] },
        { model: this.socialLink, include: [{ model: this.reviewPlatform }] },
      ],
    });

    if (!findProfile) throw new HttpException(404, 'No Restaurant found');

    const restaurantCoordinates: Coordinates = {
      latitude: findProfile.latitude,
      longitude: findProfile.longitude,
    };

    const userCoordinates: Coordinates = {
      latitude,
      longitude,
    };
    const distance = calcCoordsDistance(userCoordinates, restaurantCoordinates) * 1000;

    if (distance > 50) {
      throw new HttpException(400, 'Restaurant is too far away');
    }

    let finalObj = JSON.parse(JSON.stringify(findProfile));
    if (finalObj) {
      finalObj['SocialLinks'] = await finalObj?.SocialLinks?.filter(profile => profile.isSelected);
    }
    return finalObj;
  }
}

export default RestaurantProfileService;
