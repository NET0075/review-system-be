import DB from '@databases';
import { RegisterUserDto, LoginUserDto } from '@dtos/users.dto';
import { HttpException } from '@exceptions/HttpException';
import { User } from '@interfaces/users.interface';
import { isEmpty } from '@utils/util';
import crypto from 'crypto';
import jwt from 'jsonwebtoken';
import bcrypt from 'bcrypt';
import { CLIENT_URL, SECRET_KEY } from '@config';
import { MailPayload } from '@/interfaces/mailPayload.interface';
import EmailService from './email.service';

// function convertName(input) {
//   const words = input.split(' ');

//   if (words.length === 1) {
//     return words[0].slice(0, 3).toLowerCase();
//   } else if (words.length === 2) {
//     const firstPart = words[0].slice(0, 3).toLowerCase();
//     const secondPart = words[1][0].toLowerCase();
//     return firstPart + secondPart;
//   } else {
//     const initials = words.map(word => (word.length > 0 ? word[0].toLowerCase() : ''));
//     return initials.join('');
//   }
// }

class AuthService {
  public users = DB.User;
  public accountHash = DB.AccountVerification;
  private accessTokenExpiry: number = 60 * 60 * 24;
  public refreshToken = DB.RefreshToken;
  public passwordToken = DB.ResetPasswordToken;
  public profile = DB.RestaurantProfile;
  public role = DB.Role;
  public emailService = new EmailService();

  public async signup(userData: RegisterUserDto): Promise<{ accessToken: string; refreshToken; user: User }> {
    if (isEmpty(userData)) throw new HttpException(400, 'userData is empty');

    const findUser: User = await this.users.findOne({
      where: DB.Sequelize.and({ email: userData.email }),
    });
    if (findUser) throw new HttpException(409, `This email ${userData.email} already exists`);

    // const name = convertName(userData.fullName); // Call the convertName function
    // console.log(name);
    const findSuperAdmin = await this.role.findOne({
      where: {
        role: 'SuperAdmin',
      },
    });
    const findRole = await this.role.findOne({
      where: {
        role: 'Restaurant',
      },
    });
    if (!findRole) throw new HttpException(404, 'No Role Found');
    const createUserData: User = await this.users.create({
      ...userData,
      // id: name,
      roleId: findRole.id,
    });
    const createProfile = await this.profile.create({
      userId: createUserData.id,
    });

    const token = crypto.randomBytes(20).toString('hex');
    await this.accountHash.create({
      hash: token,
      user_id: createUserData.id,
    });

    const accessToken = jwt.sign({ id: createUserData.id }, SECRET_KEY, {
      expiresIn: this.accessTokenExpiry,
    });
    const refreshToken = await this.refreshToken.createToken(createUserData);
    if (createProfile) {
      const mailData: MailPayload = {
        templateData: {
          fullName: createUserData.fullName,
          adminName: findSuperAdmin.fullName,
        },
        mailerData: {
          to: findSuperAdmin.email,
        },
      };
      this.emailService.sendNewAccountMail(mailData);
    }

    return {
      accessToken,
      refreshToken,
      user: {
        id: createUserData.id,
        fullName: createUserData.fullName,
        email: createUserData.email,
        isEmailVerified: createUserData.isEmailVerified,
        roleId: createUserData.roleId,
      },
    };
  }

  public async login(userData: LoginUserDto): Promise<{
    accessToken: string;
    refreshToken: string;
    user: User;
  }> {
    let refreshToken = null;

    if (isEmpty(userData)) throw new HttpException(400, 'userData is empty');

    const findUser = await this.users.findOne({
      where: { email: userData.email, isApproved: 'Active' },
      include: { model: this.role },
    });
    if (!findUser) throw new HttpException(409, `This email ${userData.email} was not found or request haven't accepted yet`);

    const isPasswordMatching = await bcrypt.compare(userData.password, findUser.password);
    if (!isPasswordMatching) {
      throw new HttpException(409, 'Wrong Password');
    }

    const token = jwt.sign({ id: findUser.id }, SECRET_KEY, {
      expiresIn: this.accessTokenExpiry,
    });
    if (!refreshToken) {
      refreshToken = await this.refreshToken.createToken(findUser);
    }

    return {
      accessToken: token,
      refreshToken,
      user: findUser,
    };
  }
  public async forgotPassword(email: string) {
    const emailRecord = await this.users.findOne({
      where: { email },
    });
    if (!emailRecord) throw new HttpException(404, 'Account not found');
    const token = crypto.randomBytes(20).toString('hex');
    await this.passwordToken.createToken(token, emailRecord);

    const mailData: MailPayload = {
      templateData: {
        firstName: emailRecord.firstName,
        link: `${CLIENT_URL}/authentication/confirmPassword//${token}`,
      },
      mailerData: {
        to: email,
      },
    };
    await this.emailService.forgotPassword(mailData);
    return emailRecord;
  }

  public async verifyPasswordtoken(token: string) {
    const record = await this.passwordToken.findOne({
      where: { hash: token },
    });
    if (!record) throw new HttpException(404, 'Invalid token');
    const isInvalid = await this.passwordToken.verifyToken(record);
    if (isInvalid) throw new HttpException(400, 'Token is Expired');
    await isInvalid.destroy({ force: true });
    return { message: 'Valid Token' };
  }

  public async resetPassword(token: string, newPassword: string) {
    const record = await this.passwordToken.findOne({
      where: { hash: token },
    });
    if (!record) throw new HttpException(400, 'link has been expired');
    const findUser = await this.users.findOne({
      where: { id: record?.user_id },
    });

    const isOldPasswordMatch = await bcrypt.compare(newPassword, findUser.password);

    if (isOldPasswordMatch) {
      throw new HttpException(400, 'New password should not be same as old password');
    }
    const userRecord = await this.users.update({ password: newPassword }, { where: { id: record.user_id } });
    if (!userRecord) throw new HttpException(500, 'Error occurred try again');
    record.destroy();
    return { message: 'Password reset successfully' };
  }

  public async togglePublishRestaurant({ trainer, restaurantId }): Promise<{ count: number }> {
    const RestaurantRecord = await this.users.findOne({
      where: {
        id: restaurantId,
      },
    });
    if (!RestaurantRecord) throw new HttpException(403, 'Forbidden Resource');

    const status = RestaurantRecord.isApproved === 'Pending' || RestaurantRecord.isApproved === 'InActive' ? 'Active' : 'InActive';
    const res = await RestaurantRecord.update({ isApproved: status });
    let count = res.isApproved === 'InActive' ? 0 : 1;
    const mailData: MailPayload = {
      templateData: {
        status: res.isApproved,
        fullName: RestaurantRecord.fullName,
      },
      mailerData: {
        to: RestaurantRecord.email,
      },
    };
    this.emailService.sendRestaurantToggleMail(mailData);
    return { count };
  }
}

export default AuthService;
