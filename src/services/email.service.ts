import nodemailer from 'nodemailer';
import { SMTP_USERNAME, SMTP_PASSWORD, SMTP_EMAIL_FROM } from '@config';
import { Options } from 'nodemailer/lib/smtp-transport';
import path from 'path';
import * as ejs from 'ejs';
import { MailPayload } from '@/interfaces/mailPayload.interface';

const transporterOptions: Options = {
  service: 'gmail',
  auth: {
    user: SMTP_USERNAME,
    pass: SMTP_PASSWORD,
  },
};

class EmailService {
  private transporter: nodemailer.Transporter;
  // Initialise transporter instance;
  private async createConnection() {
    this.transporter = nodemailer.createTransport(transporterOptions);
  }
  private async terminateConnection() {
    this.transporter.close();
  }
  /**
   * Sends verification email for account activation
   * @param payload
   */

  public async forgotPassword(payload: MailPayload) {
    try {
      await this.createConnection();
      await this.transporter.verify();

      const pathToView = path.resolve(__dirname, '../view/forgotPassword.ejs');
      const { templateData, mailerData } = payload;

      ejs.renderFile(pathToView, templateData, async (err, data) => {
        try {
          await this.transporter.sendMail({
            from: `Food-Genie: ${SMTP_EMAIL_FROM}`,
            to: mailerData.to,
            subject: 'Passwrod Reset',
            html: data,
          });
          this.terminateConnection();
        } catch (error) {
          return error;
        }
      });
    } catch (err) {
      return err;
    }
  }
  public async forgotCustomerPassword(payload: MailPayload) {
    try {
      await this.createConnection();
      await this.transporter.verify();

      const pathToView = path.resolve(__dirname, '../view/forgotCustomerPassword.ejs');
      const { templateData, mailerData } = payload;

      ejs.renderFile(pathToView, templateData, async (err, data) => {
        try {
          await this.transporter.sendMail({
            from: `Food-Genie: ${SMTP_EMAIL_FROM}`,
            to: mailerData.to,
            subject: 'Password Reset',
            html: data,
          });
          this.terminateConnection();
        } catch (error) {
          return error;
        }
      });
    } catch (err) {
      return err;
    }
  }
  public async sendRestaurantToggleMail(payload: MailPayload) {
    try {
      await this.createConnection();
      await this.transporter.verify();

      const pathToView = path.resolve(__dirname, '../view/restaurantToggleMail.ejs');
      const { templateData, mailerData } = payload;

      ejs.renderFile(pathToView, templateData, async (err, data) => {
        try {
          await this.transporter.sendMail({
            from: `Food-Genie: ${SMTP_EMAIL_FROM}`,
            to: mailerData.to,
            subject: 'Account Status',
            html: data,
          });
          this.terminateConnection();
        } catch (error) {
          console.log(error);
        }
      });
    } catch (err) {
      return err;
    }
  }

  public async sendNewAccountMail(payload: MailPayload) {
    try {
      await this.createConnection();
      await this.transporter.verify();

      // Mailing Data assignment
      const pathToView = path.resolve(__dirname, '../view/accountAddedMail.ejs');
      const { templateData, mailerData } = payload;

      ejs.renderFile(pathToView, templateData, async (err, data) => {
        try {
          await this.transporter.sendMail({
            from: `Food Genie: ${SMTP_EMAIL_FROM}`,
            to: mailerData.to,
            subject: 'New Restaurant ',
            html: data,
            // attachments: [
            //   {
            //     filename: 'Stembotix_logo.png',
            //     path: __dirname + '/../public/assets/Stembotix_logo.png',
            //     cid: 'logo@cid',
            //   },
            // ],
          });
          this.terminateConnection();
        } catch (error) {
          return error;
        }
      });
    } catch (err) {
      return err;
    }
  }
}
export default EmailService;
