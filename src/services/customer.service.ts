import DB from '@databases';
import { HttpException } from '@exceptions/HttpException';
import { User } from '@interfaces/users.interface';
import { isEmpty } from '@utils/util';
import { Op } from 'sequelize';

class CustomerService {
  public customer = DB.Customer;

  public isAdmin(userData): boolean {
    return userData.role === 'SuperAdmin' || userData.role === 'Admin';
  }

  public async findAllCustomer(queryObject): Promise<{ totalCount: number; records: User | undefined }> {
    const sortBy = queryObject.sortBy ? queryObject.sortBy : 'createdAt';
    const order = queryObject.order || 'DESC';
    // pagination
    const pageSize = queryObject.pageRecord ? queryObject.pageRecord : 10;
    const pageNo = queryObject.pageNo ? (queryObject.pageNo - 1) * pageSize : 0;

    // Search
    const [search, searchCondition] = queryObject.search ? [`%${queryObject.search}%`, DB.Sequelize.Op.iLike] : ['', DB.Sequelize.Op.ne];

    const allCustomer = await this.customer.findAndCountAll({
      where: {
        [Op.or]: [
          {
            firstName: {
              [searchCondition]: search,
            },
          },
          {
            lastName: {
              [searchCondition]: search,
            },
          },
        ],
      },
      distinct: true,
      limit: pageSize,
      offset: pageNo,
      order: [[`${sortBy}`, `${order}`]],
    });
    return { totalCount: allCustomer.count, records: allCustomer.rows };
  }

  public async findCustomerById(customerId: string) {
    if (isEmpty(customerId)) throw new HttpException(400, 'CustomerId is empty');

    const findCustomer = await this.customer.findOne({
      where: {
        id: customerId,
      },
    });
    if (!findCustomer) throw new HttpException(409, "Customer doesn't exist");
    return findCustomer;
  }

  public async deleteCustomer(customerId: string): Promise<User> {
    if (isEmpty(customerId)) throw new HttpException(400, "Customer doesn't existId");

    const findCustomer = await this.customer.findOne({ where: { id: customerId } });
    if (!findCustomer) throw new HttpException(409, "Customer doesn't exist");
    await findCustomer.destroy();

    return findCustomer;
  }
}

export default CustomerService;
