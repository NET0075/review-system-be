import { hash } from 'bcrypt';
import DB from '@databases';
import { HttpException } from '@exceptions/HttpException';
import { User } from '@interfaces/users.interface';
import { Op } from 'sequelize';

class StaffService {
  public users = DB.User;
  public profile = DB.RestaurantProfile;
  public review = DB.Review;
  public socialLinks = DB.SocialLinks;
  public platform = DB.ReviewPlatform;
  public menuCategory = DB.Categories;
  public menu = DB.Menu;
  public role = DB.Role;

  public async getAllStaffList(queryObject): Promise<{ totalCount: number; records: undefined }> {
    const sortBy = queryObject.sortBy ? queryObject.sortBy : 'createdAt';
    const order = queryObject.order || 'DESC';
    // pagination
    const pageSize = queryObject.pageRecord ? queryObject.pageRecord : 10;
    const pageNo = queryObject.pageNo ? (queryObject.pageNo - 1) * pageSize : 0;
    // Search
    const [search, searchCondition] = queryObject.search ? [`%${queryObject.search}%`, DB.Sequelize.Op.iLike] : ['', DB.Sequelize.Op.ne];

    const excludedRoles = ['SuperAdmin', 'Restaurant'];
    const findRole = await this.role.findAll({
      where: { role: { [Op.notIn]: excludedRoles } },
    });
    const staffId = findRole.map((role: { id: string }) => role.id);

    const findStaff = await this.users.findAndCountAll({
      where: {
        roleId: staffId,
        [Op.or]: [{ fullName: { [searchCondition]: search } }, { email: { [searchCondition]: search } }],
      },
      include: [
        {
          model: this.role,
        },
      ],
      limit: pageSize,
      offset: pageNo,
      order: [[`${sortBy}`, `${order}`]],
    });
    return { totalCount: findStaff.count, records: findStaff.rows };
  }

  public async getStaffById(staffId) {
    const findStaff = await this.users.findOne({ where: { id: staffId } });
    if (!findStaff) throw new HttpException(404, 'No staff found');
    return findStaff;
  }

  public async createStaff({ staffData }) {
    const findUser: User = await this.users.findOne({ where: { email: staffData.email } });
    if (findUser) throw new HttpException(409, `This email ${staffData.email} already exists`);

    const findRole = await this.role.findOne({
      where: { id: staffData.roleId, deletedAt: null },
    });
    if (!findRole) throw new HttpException(404, 'No role found');

    const createStaffData: User = await this.users.create({
      fullName: staffData.fullName,
      email: staffData.email,
      password: staffData.password,
      isApproved: 'Active',
      roleId: staffData.roleId,
    });

    return createStaffData;
  }

  public async updateStaff({ staffId, staffData }) {
    const findStaff = await this.users.findOne({ where: { id: staffId } });

    if (!findStaff) throw new HttpException(404, 'No staff found');

    await findStaff.update(
      {
        ...staffData,
      },
      { where: { id: staffId }, returning: true },
    );
    return findStaff;
  }

  public async deleteStaff(staffId) {
    const findStaff = await this.users.findOne({ where: { id: staffId } });

    if (!findStaff) throw new HttpException(404, 'No staff found');

    await findStaff.destroy({ where: { id: staffId } });
    return findStaff;
  }
}

export default StaffService;
