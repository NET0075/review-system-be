import DB from '@databases';
import { HttpException } from '@exceptions/HttpException';
import { Op } from 'sequelize';

class CategoriesService {
  public users = DB.User;
  public profile = DB.RestaurantProfile;
  public menuCategories = DB.Categories;
  public menu = DB.Menu;

  public isAdmin(userData): boolean {
    return userData.role === 'SuperAdmin' || userData.role === 'Admin';
  }

  public async createMenuCategories({ categoryData, file, user }) {
    const thumbnail = file;
    const thumbnailPath = `/media/${thumbnail.path.split('/').splice(-2).join('/')}`;

    // const thumbnailPath = `/media/${file.path.split('/').splice(-2).join('/')}`;

    const findCategory = await this.menuCategories.findOne({
      where: DB.Sequelize.and({
        catName: {
          [Op.iLike]: `%${categoryData.catName}%`,
        },
        userId: categoryData.userId,
      }),
    });
    if (findCategory) throw new HttpException(409, 'Category already exist');

    const createCategory = await this.menuCategories.create({
      ...categoryData,
      thumbnail: thumbnailPath,
    });
    return createCategory;
  }
  public async getAllMenuCategories(queryObject) {
    const sortBy = queryObject.sortBy ? queryObject.sortBy : 'createdAt';
    const order = queryObject.order || 'DESC';
    // pagination
    const pageSize = queryObject.pageRecord ? queryObject.pageRecord : 10;
    const pageNo = queryObject.pageNo ? (queryObject.pageNo - 1) * pageSize : 0;

    // Search
    const [search, searchCondition] = queryObject.search ? [`%${queryObject.search}%`, DB.Sequelize.Op.iLike] : ['', DB.Sequelize.Op.ne];

    const status = queryObject?.isApproved || null;
    const restId = queryObject?.userId || null;

    const response = await this.menuCategories.findAndCountAll({
      where: DB.Sequelize.and(
        {
          catName: {
            [searchCondition]: search,
          },
        },
        status !== null
          ? {
              status: status,
            }
          : null,
        restId !== null
          ? {
              userId: restId,
            }
          : null,
      ),

      include: [{ model: this.menu }],
      distinct: true,
      limit: pageSize,
      offset: pageNo,
      order: [[`${sortBy}`, `${order}`]],
    });

    const findCategory = await this.menuCategories.findAll({ where: { userId: restId } });

    const activeCount = await findCategory.filter(count => count.status === 'Active').length;
    const inActiveCount = await findCategory.filter(count => count.status === 'InActive').length;
    return { totalCount: response.count, records: response.rows, activeCount, inActiveCount };
  }

  public async getAllMenuCategoriesCustomer(queryObject) {
    const status = 'Active';
    const restId = queryObject?.userId || null;

    const response = await this.menuCategories.findAll({
      where: DB.Sequelize.and(
        status !== null
          ? {
              status: status,
            }
          : null,
        restId !== null
          ? {
              userId: restId,
            }
          : null,
      ),

      include: [{ model: this.menu }],
      distinct: true,
    });
    return { records: response };
  }

  public async getAllMenuCategoriesById({ queryObject, categoryId }): Promise<{ totalCount: number; records: undefined }> {
    const sortBy = queryObject.sortBy ? queryObject.sortBy : 'createdAt';
    const order = queryObject.order || 'DESC';
    // pagination
    const pageSize = queryObject.pageRecord ? queryObject.pageRecord : 10;
    const pageNo = queryObject.pageNo ? (queryObject.pageNo - 1) * pageSize : 0;

    // Search
    const [search, searchCondition] = queryObject.search ? [`%${queryObject.search}%`, DB.Sequelize.Op.iLike] : ['', DB.Sequelize.Op.ne];
    const response = await this.menuCategories.findAndCountAll({
      where: DB.Sequelize.and({
        id: categoryId,
        catName: {
          [searchCondition]: search,
        },
      }),
      include: [{ model: this.menu }],
      distinct: true,
      limit: pageSize,
      offset: pageNo,
      order: [[`${sortBy}`, `${order}`]],
    });
    return { totalCount: response.count, records: response.rows };
  }

  public async updateMenuCategories({ categoryData, categoryId, file, user }) {
    const findCategory = await this.menuCategories.findOne({
      where: { id: categoryId },
    });
    if (!findCategory) throw new HttpException(404, 'No data found');
    if (file) {
      const thumbnail = file;
      const thumbnailPath = `/media/${thumbnail.path.split('/').splice(-2).join('/')}`;

      // const thumbnailPath = `/media/${file.path.split('/').splice(-2).join('/')}`;

      const updateCategory = await this.menuCategories.update(
        {
          ...categoryData,
          thumbnail: thumbnailPath,
        },
        {
          where: { id: categoryId },
        },
      );
      return updateCategory;
    } else {
      const updateCategory = await this.menuCategories.update(
        {
          ...categoryData,
        },
        {
          where: { id: categoryId },
        },
      );
      return updateCategory;
    }
  }
  public async deleteMenuCategory({ categoryId, user }) {
    const findCategory = await this.menuCategories.findOne({
      where: {
        id: categoryId,
      },
    });
    if (!findCategory) {
      throw new HttpException(404, 'No data found');
    }
    if (findCategory.status === 'Active') {
      throw new HttpException(409, 'Error: Deletion of Active Category is not permitted. \uD83D\uDE41');
    } else {
      await findCategory.destroy({
        include: [
          {
            model: this.menu,
          },
        ],
      });
      return findCategory;
    }
  }

  public async togglePublishCategory({ trainer, categoryId }): Promise<{ count: number }> {
    const CategoryRecord = await this.menuCategories.findOne({
      where: {
        id: categoryId,
      },
    });
    if (!CategoryRecord) throw new HttpException(404, 'No Category found');
    // const findRestaurant = await this.users.findOne({
    //   where: {
    //     id: CategoryRecord.userId,
    //   },
    // });

    const status = CategoryRecord.status === 'Pending' || CategoryRecord.status === 'InActive' ? 'Active' : 'InActive';
    const res = await CategoryRecord.update({ status: status });
    let count = res.status === 'InActive' ? 0 : 1;
    // const mailData: MailPayload = {
    //   templateData: {
    //     status: res.status,
    //   },
    //   mailerData: {
    //     to: findRestaurant.email,
    //   },
    // };
    // this.emailService.sendRestaurantToggleMail(mailData);
    return { count };
  }
}

export default CategoriesService;
