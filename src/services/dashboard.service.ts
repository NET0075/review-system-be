import DB from '@databases';
import moment from 'moment';
import { Op } from 'sequelize';

class DashboardService {
  public users = DB.User;
  public customer = DB.Customer;
  public review = DB.Review;
  public role = DB.Role;

  public isAdmin(userData): boolean {
    return userData.role === 'SuperAdmin' || userData.role === 'Admin';
  }

  public async getCount(): Promise<{ restaurantCount: number; customerCount: number }> {
    const findRestaurant = await this.role.findOne({
      where: { role: 'Restaurant' },
    });
    const findAllRestaurantCount = await this.users.findAndCountAll({ where: { roleId: findRestaurant.id, deletedAt: null, isApproved: 'Active' } });
    const findAllCoustomerCount = await this.customer.findAndCountAll({ where: { deletedAt: null } });
    return { restaurantCount: findAllRestaurantCount.count, customerCount: findAllCoustomerCount.count };
  }

  public async getCustomerCount({ user }): Promise<{ reviewCount: number }> {
    const findAllReviewData = await this.review.findAndCountAll({ where: { userId: user.id } });
    return { reviewCount: findAllReviewData.count };
  }
  public async getAllRestaurantMonthWise(user) {
    const findRole = await this.role.findOne({ where: { role: 'Restaurant' } });
    const count = {};
    for (let i = 1; i <= 12; i++) {
      const recordsPerMonth = await this.users.findAndCountAll({
        where: {
          createdAt: {
            [Op.gte]: moment()
              .month(i - 1)
              .startOf('month')
              .year(moment().year())
              .toDate(),

            [Op.lt]: moment()
              .month(i - 1)
              .endOf('month')
              .year(moment().year())
              .toDate(),
          },
          roleId: {
            [Op.eq]: findRole.id,
          },
          isApproved: 'Active',
        },
      });

      const recordPerMonth = await this.customer.findAndCountAll({
        where: {
          createdAt: {
            [Op.gte]: moment()
              .month(i - 1)
              .startOf('month')
              .year(moment().year())
              .toDate(),

            [Op.lt]: moment()
              .month(i - 1)
              .endOf('month')
              .year(moment().year())
              .toDate(),
          },
        },
      });

      const monthName = moment()
        .month(i - 1)
        .format('MMMM');
      count[monthName] = {
        restaurantCount: recordsPerMonth.count,
        customerCount: recordPerMonth.count,
      };
    }
    return { count };
  }
}

export default DashboardService;
