import DB from '@databases';
import { HttpException } from '@exceptions/HttpException';
import EmailService from './email.service';
import { LoginCustomerDTO, RegisterCustomerDto } from '@/dtos/customer.dto';
import { isEmpty } from '@/utils/util';
import { Customer } from '@/interfaces/customerAuth.interface';
import crypto from 'crypto';
import { SECRET_KEY } from '@/config';
import jwt from 'jsonwebtoken';

function generate4DigitOTP() {
  const min = 1000;
  const max = 9999;
  return Math.floor(Math.random() * (max - min + 1) + min);
}

class CustomerAuthService {
  public customer = DB.Customer;
  public accountHash = DB.AccountVerification;
  private accessTokenExpiry: number = 60 * 60 * 24;
  public refreshToken = DB.RefreshToken;
  public passwordToken = DB.ResetPasswordToken;
  public otp = DB.Otp;
  public emailService = new EmailService();



  public async signup(customerData: RegisterCustomerDto): Promise<{ accessToken: string; refreshToken; customer: Customer }> {
    if (isEmpty(customerData)) throw new HttpException(400, 'Customer Data is empty');

    const findCustomer: Customer = await this.customer.findOne({
      where: DB.Sequelize.and({ email: customerData.email }),
    });
    if (findCustomer) throw new HttpException(409, `This email ${customerData.email} already exists`);

    const createCustomerData: Customer = await this.customer.create({
      ...customerData,
    });

    const token = crypto.randomBytes(20).toString('hex');
    await this.accountHash.create({
      hash: token,
      user_id: createCustomerData.id,
    });

    const accessToken = jwt.sign({ id: createCustomerData.id }, SECRET_KEY, {
      expiresIn: this.accessTokenExpiry,
    });
    const refreshToken = await this.refreshToken.createToken({ ...createCustomerData });

    return {
      accessToken,
      refreshToken,
      customer: {
        id: createCustomerData.id,
        firstName: createCustomerData.firstName,
        lastName: createCustomerData.lastName,
        email: createCustomerData.email,
        number: createCustomerData.number,
      },
    };
  }

  public async login({ mobileNumber }: LoginCustomerDTO) {
    let otpData = await this.otp.findOne({
      where: {
        number: mobileNumber,
      },
    });
    const findCustomer = await this.customer.findOne({
      where: {
        number: mobileNumber,
      },
    });
    if (!findCustomer) throw new HttpException(404, 'No User Found');
    if (otpData) {
      await this.otp.destroy({
        where: {
          number: mobileNumber,
        },
        force: true,
      });
    }
    const newOTP = generate4DigitOTP();
    await this.otp.create({
      otp: newOTP,
      number: mobileNumber,
    });
    return { otp: newOTP };
  }

  public async validateOtp({ mobileNumber, otp }) {
    const findCustomer = await this.customer.findOne({
      where: {
        number: mobileNumber,
      },
    });
    if (findCustomer) {
      const validateOtp = await this.otp.findOne({
        where: DB.Sequelize.and({ number: mobileNumber, otp }),
      });
      if (validateOtp) {
        validateOtp.destroy({ force: true });
        return { data: findCustomer, message: 'Logged in Successfully', status: true };
      } else {
        return { message: 'Invalid Otp' };
      }
    } else {
      return { message: 'No User Found', status: false };
    }
  }
}

export default CustomerAuthService;
