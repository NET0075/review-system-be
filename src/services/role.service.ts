import DB from '@databases';
import { HttpException } from '@exceptions/HttpException';
import { Op } from 'sequelize';

class RoleService {
  public users = DB.User;
  public profile = DB.RestaurantProfile;
  public menuCategories = DB.Categories;
  public menu = DB.Menu;
  public role = DB.Role;

  public isAdmin(userData): boolean {
    return userData.role === 'SuperAdmin' || userData.role === 'Admin';
  }

  public async getAllRole(queryObject): Promise<{ totalCount: number; records: undefined[] }> {
    const sortBy = queryObject.sortBy ? queryObject.sortBy : 'createdAt';
    const order = queryObject.order || 'DESC';
    // pagination
    const pageSize = queryObject.pageRecord ? queryObject.pageRecord : 10;
    const pageNo = queryObject.pageNo ? (queryObject.pageNo - 1) * pageSize : 0;
    // Search
    const [search, searchCondition] = queryObject.search ? [`%${queryObject.search}%`, DB.Sequelize.Op.iLike] : ['', DB.Sequelize.Op.ne];

    const findRole = await this.role.findAndCountAll({
      where: DB.Sequelize.and({
        role: {
          [searchCondition]: search,
        },
      }),
      limit: pageSize,
      offset: pageNo,
      order: [[`${sortBy}`, `${order}`]],
    });
    if (!findRole) throw new HttpException(404, 'No role found');

    return { totalCount: findRole.count, records: findRole.rows };
  }
  public async findAllRoles() {
    const findRole = await this.role.findAll({
      order: [['role', 'ASC']],
    });
    if (!findRole) throw new HttpException(404, 'No role found');

    return findRole;
  }

  public async getRolebyId(roleId) {
    const findRole = await this.role.findOne({ where: { id: roleId } });
    if (!findRole) throw new HttpException(404, 'No role found');

    return findRole;
  }

  public async createRole({ roleData, user }) {
    const findRole = await this.role.findOne({
      where: DB.Sequelize.and({
        role: {
          [Op.iLike]: `%${roleData.role}%`,
        },
      }),
    });
    if (findRole) throw new HttpException(409, 'Role already exist');

    const createRole = await this.role.create({
      ...roleData,
    });
    return createRole;
  }
  public async updateRole({ roleData, roleId }) {
    const findRole = await this.role.findOne({
      where: { id: roleId },
    });
    if (!findRole) throw new HttpException(404, 'No data found');

    const updateRole = await this.role.update(
      {
        ...roleData,
      },
      {
        where: { id: roleId },
        returning: true,
      },
    );
    return updateRole[1];
  }

  public async deleteRole({ roleId }) {
    const findRole = await this.role.findOne({
      where: {
        id: roleId,
      },
    });
    if (!findRole) {
      throw new HttpException(404, 'No data found');
    }

    const findUser = await this.users.findOne({ where: { roleId, deletedAt: null } });
    if (findUser) throw new HttpException(409, 'Users exist with this role');

    await findRole.destroy();
    return findRole;
  }
}

export default RoleService;
