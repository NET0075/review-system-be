import { HttpException } from '@/exceptions/HttpException';
import DB from '@databases';

class ReviewPlatformService {
  public user = DB.User;
  public reviewPlatform = DB.ReviewPlatform;

  public async createReviewPlatform({ platformData, file, user }) {
    const thumbnail = file;
    const thumbnailPath = `/media/${thumbnail.path.split('/').splice(-2).join('/')}`;

    // const thumbnailPath = `/media/${file.path.split('/').splice(-2).join('/')}`;

    const createPlatform = await this.reviewPlatform.create({
      ...platformData,
      logo: thumbnailPath,
    });
    return createPlatform;
  }

  public async getAllPlatform({ queryObject }): Promise<{ totalCount: number; records: undefined }> {
    const sortBy = queryObject.sortBy ? queryObject.sortBy : 'createdAt';
    const order = queryObject.order || 'DESC';
    // pagination
    const pageSize = queryObject.pageRecord ? queryObject.pageRecord : 10;
    const pageNo = queryObject.pageNo ? (queryObject.pageNo - 1) * pageSize : 0;
    // Search
    const [search, searchCondition] = queryObject.search ? [`%${queryObject.search}%`, DB.Sequelize.Op.iLike] : ['', DB.Sequelize.Op.ne];
    const findPlatform = await this.reviewPlatform.findAndCountAll({ limit: pageSize, offset: pageNo, order: [[`${sortBy}`, `${order}`]] });
    return { totalCount: findPlatform.count, records: findPlatform.rows };
  }

  public async getAllPlatforms(): Promise<{ totalCount: number; records: undefined }> {
    const findPlatform = await this.reviewPlatform.findAndCountAll({
      order: [[`createdAt`, 'DESC']],
    });
    return { totalCount: findPlatform.count, records: findPlatform.rows };
  }

  public async getPlatformById({ platformId }) {
    const findPlatform = await this.reviewPlatform.findOne({
      where: { id: platformId },
    });
    if (!findPlatform) throw new HttpException(404, 'No data found');
    return findPlatform;
  }

  public async updatePlatform({ platformData, platformId, user, file }) {
    const findPlatform = await this.reviewPlatform.findOne({
      where: { id: platformId },
    });
    if (!findPlatform) throw new HttpException(404, 'No data found');
    let updatePlatform;

    if (file) {
      const thumbnail = file;
      const thumbnailPath = `/media/${thumbnail.path.split('/').splice(-2).join('/')}`;

      // const thumbnailPath = `/media/${file.path.split('/').splice(-2).join('/')}`;

      updatePlatform = await this.reviewPlatform.update(
        {
          ...platformData,
          logo: thumbnailPath,
        },
        { where: { id: platformId }, returning: true },
      );
    } else {
      updatePlatform = await this.reviewPlatform.update(
        {
          ...platformData,
        },
        { where: { id: platformId }, returning: true },
      );
    }
    return updatePlatform[1];
  }

  public async deletePlatform({ platformId, user }) {
    const findPlatform = await this.reviewPlatform.findOne({
      where: { id: platformId },
    });
    if (!findPlatform) throw new HttpException(404, 'No data found');
    await findPlatform.destroy();
    return findPlatform;
  }
}
export default ReviewPlatformService;
