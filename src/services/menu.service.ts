import DB from '@databases';
import { HttpException } from '@exceptions/HttpException';
import { isEmpty } from '@utils/util';
import { Op } from 'sequelize';
import { getCountry, getCurrency } from 'country-currency-map';

class MenuService {
  public users = DB.User;
  public profile = DB.RestaurantProfile;
  public menuCategories = DB.Categories;
  public menu = DB.Menu;

  public isAdmin(userData): boolean {
    return userData.role === 'SuperAdmin' || userData.role === 'Admin';
  }

  public async getAllMenus(queryObject): Promise<{ totalCount: number; records: undefined }> {
    const sortBy = queryObject.sortBy ? queryObject.sortBy : 'createdAt';
    const order = queryObject.order || 'DESC';
    // pagination
    const pageSize = queryObject.pageRecord ? queryObject.pageRecord : 10;
    const pageNo = queryObject.pageNo ? (queryObject.pageNo - 1) * pageSize : 0;

    // Search
    const [search, searchCondition] = queryObject.search ? [`%${queryObject.search}%`, DB.Sequelize.Op.iLike] : ['', DB.Sequelize.Op.ne];

    const status = queryObject?.isApproved || null;
    const menuCategoryId = queryObject?.menuCategoryId || null;

    const response = await this.menu.findAndCountAll({
      where: DB.Sequelize.and(
        {
          name: {
            [searchCondition]: search,
          },
        },
        status !== null
          ? {
              status: status,
            }
          : null,
        menuCategoryId !== null
          ? {
              categoryId: menuCategoryId,
            }
          : null,
      ),
      distinct: true,
      limit: pageSize,
      offset: pageNo,
      order: [[`${sortBy}`, `${order}`]],
    });
    return { totalCount: response.count, records: response.rows };
  }

  public async createMenu({ menuData, user, file }) {
    if (isEmpty(menuData)) throw new HttpException(400, 'RestaurantData is empty');

    const findCategory = await this.menuCategories.findOne({ where: { id: menuData.categoryId } });
    if (!findCategory) throw new HttpException(404, 'No data found');

    const findRestaurantProfile = await this.profile.findOne({
      where: {
        userId: findCategory.userId,
      },
    });

    if (!findRestaurantProfile) throw new HttpException(409, 'Forbidden Resources');

    let country = getCountry(findRestaurantProfile.country);
    let currency = getCurrency(country.currency);
    let cleanSymbolFormat = currency.symbolFormat.replace(/\{#\}/g, '');

    const findMenu = await this.menu.findOne({
      where: {
        name: {
          [Op.iLike]: `%${menuData.name}%`,
        },
      },
    });
    if (findMenu) throw new HttpException(409, 'Menu already exist');
    const thumbnailPath = `/media/${file.path.split('/').splice(-2).join('/')}`;

    const createCategory = await this.menu.create({
      ...menuData,
      isVeg: JSON.parse(menuData.isVeg),
      isJain: JSON.parse(menuData.isJain),
      isSpicy: JSON.parse(menuData.isSpicy),
      isNonVeg: JSON.parse(menuData.isNonVeg),
      isHalf: JSON.parse(menuData.isHalf),
      isEgg: JSON.parse(menuData.isEgg),
      currencySymbol: cleanSymbolFormat,
      thumbnail: thumbnailPath,
    });
    return createCategory;
  }

  public async updateMenu({ menuData, menuId, user, file }) {
    const findCategory = await this.menu.findOne({
      where: { id: menuId },
    });
    if (!findCategory) throw new HttpException(404, 'No data found');
    if (file) {
      const thumbnailPath = `/media/${file.path.split('/').splice(-2).join('/')}`;

      const updateCategory = await this.menu.update(
        {
          ...menuData,
          isVeg: JSON.parse(menuData.isVeg),
          isJain: JSON.parse(menuData.isJain),
          isSpicy: JSON.parse(menuData.isSpicy),
          isNonVeg: JSON.parse(menuData.isNonVeg),
          isHalf: JSON.parse(menuData.isHalf),
          isEgg: JSON.parse(menuData.isEgg),
          thumbnail: thumbnailPath,
        },
        {
          where: { id: menuId },
          returning: true,
        },
      );
      return updateCategory[1];
    }

    const updateCategory = await this.menu.update(
      {
        ...menuData,
        isVeg: JSON.parse(menuData.isVeg),
        isJain: JSON.parse(menuData.isJain),
        isSpicy: JSON.parse(menuData.isSpicy),
        isNonVeg: JSON.parse(menuData.isNonVeg),
        isHalf: JSON.parse(menuData.isHalf),
        isEgg: JSON.parse(menuData.isEgg),
      },
      {
        where: { id: menuId },
        returning: true,
      },
    );
    return updateCategory[1];
  }
  public async deleteMenu({ menuId, user }) {
    const findCategory = await this.menu.findOne({
      where: {
        id: menuId,
      },
    });
    if (!findCategory) {
      throw new HttpException(404, 'No data found');
    }
    await findCategory.destroy({ force: true });
  }
}

export default MenuService;
