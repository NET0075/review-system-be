import App from '@/app';
import AuthRoute from '@routes/auth.route';
import IndexRoute from '@routes/index.route';
import RestaurantRoute from '@routes/restaurant.route';
import validateEnv from '@utils/validateEnv';
import RestaurantProfileRoute from './routes/restaurantProfile.route';
import CustomerAuthRoute from './routes/customerAuth.route';
import ReviewRoute from './routes/review.route';
import DashBoardRoute from './routes/dashBoard.route';
import CategoriesRoute from './routes/menuCategories.route';
import MenuRoute from './routes/menu.route';
import ReviewPlatformRoute from './routes/reviewPlatform.route';
import CustomerRoute from './routes/customer.route';
import RoleRoute from './routes/role.route';
import StaffRoute from './routes/staff.route';
import PaymentGatewayRoute from './routes/paymentGateway.route';

validateEnv();

const app = new App([
  new IndexRoute(),
  new RestaurantRoute(),
  new AuthRoute(),
  new RestaurantProfileRoute(),
  new CustomerAuthRoute(),
  new ReviewRoute(),
  new DashBoardRoute(),
  new CategoriesRoute(),
  new MenuRoute(),
  new ReviewPlatformRoute(),
  new PaymentGatewayRoute(),
  new CustomerRoute(),
  new RoleRoute(),
  new StaffRoute(),
]);

app.listen();
