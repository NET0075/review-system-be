import { Router } from 'express';
import AuthController from '@controllers/auth.controller';
import { RegisterUserDto, LoginUserDto, ForgotPasswordDTO, ResetPasswordDTO } from '@dtos/users.dto';
import { Routes } from '@interfaces/routes.interface';
import validationMiddleware from '@middlewares/validation.middleware';
import passport from 'passport';

class AuthRoute implements Routes {
  public path = '/auth';
  public router = Router();
  public authController = new AuthController();
  constructor() {
    this.initializeRoutes();
  }

  private initializeRoutes() {
    this.router.post(`${this.path}/signup`, this.authController.signUp);
    this.router.post(`${this.path}/login`, validationMiddleware(LoginUserDto, 'body'), this.authController.logIn);
    this.router.post(`${this.path}/refresh-token`, this.authController.refreshTokenUser);
    this.router.post(`${this.path}/forgot-password`, validationMiddleware(ForgotPasswordDTO, 'body'), this.authController.forgotPassword);
    this.router.post(`${this.path}/verify-reset-token/:token`, this.authController.verifyPasswordToken);
    this.router.post(`${this.path}/reset-password/`, validationMiddleware(ResetPasswordDTO, 'body'), this.authController.resetPassword);
    this.router.put(
      `/restaurant/toggle-publish/:restaurantId`,
      passport.authenticate('jwt', { session: false }),
      this.authController.togglePublishRestaurant,
    );
  }
}

export default AuthRoute;
