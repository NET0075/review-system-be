import { Router } from 'express';
import { Routes } from '@interfaces/routes.interface';
import validationMiddleware from '@middlewares/validation.middleware';
import RestaurantProfileController from '@/controllers/restaurantProfile.controller';
import passportConfig from '@/config/passportConfig';
import passport from 'passport';
import uploadFiles from '@/rest/fileUpload';
import { RestaurantProifle } from '@/dtos/restaurantProfile.dto';

class RestaurantProfileRoute implements Routes {
  public path = '/restaurant-profile';
  public router = Router();
  public proifleController = new RestaurantProfileController();
  public passport = passportConfig(passport);

  constructor() {
    this.initializeRoutes();
  }

  private initializeRoutes() {
    this.router.post(
      `${this.path}`,
      [passport.authenticate('jwt', { session: false }), uploadFiles.single('logo')],
      validationMiddleware(RestaurantProifle, 'body'),
      this.proifleController.createRestaurantProfile,
    );
    this.router.get(`${this.path}`, passport.authenticate('jwt', { session: false }), this.proifleController.getRestaurantProfile);
    this.router.put(
      `${this.path}/:restaurantId`,
      passport.authenticate('jwt', { session: false }),
      uploadFiles.single('logo'),
      this.proifleController.updateRestaurantProfileById,
    );
    this.router.put(
      `${this.path}`,
      passport.authenticate('jwt', { session: false }),
      uploadFiles.single('logo'),
      this.proifleController.updateRestaurantProfile,
    );
    this.router.delete(`${this.path}`, passport.authenticate('jwt', { session: false }), this.proifleController.deleteRestaurantProfile);

    this.router.delete(`${this.path}/:socialLinkId`, passport.authenticate('jwt', { session: false }), this.proifleController.deleteSocialLinks);
    this.router.get(`/getRestaurant/:qrCode/:latitude/:longitude`, this.proifleController.getRestaurantByQRCode);
  }
}

export default RestaurantProfileRoute;
