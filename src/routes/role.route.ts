import { Router } from 'express';
import { Routes } from '@interfaces/routes.interface';
import passportConfig from '@/config/passportConfig';
import passport from 'passport';
import RoleController from '@/controllers/role.controller';

class RoleRoute implements Routes {
  public path = '/role';
  public router = Router();
  public roleController = new RoleController();
  public passport = passportConfig(passport);

  constructor() {
    this.initializeRoutes();
  }

  private initializeRoutes() {
    this.router.get(`${this.path}/list`, passport.authenticate('jwt', { session: false }), this.roleController.getAllRole);
    this.router.get(`${this.path}`, passport.authenticate('jwt', { session: false }), this.roleController.findAllRoles);
    this.router.get(`${this.path}/:roleId`, passport.authenticate('jwt', { session: false }), this.roleController.getRoleById);
    this.router.post(`${this.path}`, passport.authenticate('jwt', { session: false }), this.roleController.createRole);
    this.router.put(`${this.path}/:roleId`, passport.authenticate('jwt', { session: false }), this.roleController.updateRole);
    this.router.delete(`${this.path}/:roleId`, passport.authenticate('jwt', { session: false }), this.roleController.deleteRole);
  }
}

export default RoleRoute;
