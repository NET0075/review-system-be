import { Router } from 'express';
import { Routes } from '@interfaces/routes.interface';
import CustomerAuthController from '@/controllers/customerAuth.controller';
import { RegisterCustomerDto } from '@/dtos/customer.dto';
import validationMiddleware from '@/middlewares/validation.middleware';

class CustomerAuthRoute implements Routes {
  public path = '/customer-auth';
  public router = Router();
  public customerAuthController = new CustomerAuthController();
  constructor() {
    this.initializeRoutes();
  }

  private initializeRoutes() {
    this.router.post(`${this.path}/signup`, validationMiddleware(RegisterCustomerDto, 'body'), this.customerAuthController.signUp);
    this.router.post(`${this.path}/login`, this.customerAuthController.logIn);
    this.router.post(`${this.path}/verify`, this.customerAuthController.validateOtp);
  }
}

export default CustomerAuthRoute;
