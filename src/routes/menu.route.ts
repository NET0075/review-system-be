import { Router } from 'express';
import { Routes } from '@interfaces/routes.interface';
import validationMiddleware from '@middlewares/validation.middleware';
import passportConfig from '@/config/passportConfig';
import passport from 'passport';
import MenuController from '@/controllers/menu.controller';
import { MenuDTO, MenuDto } from '@/dtos/menu.dto';
import uploadFiles from '@/rest/fileUpload';

class MenuRoute implements Routes {
  public path = '/menu';
  public router = Router();
  public menuController = new MenuController();
  public passport = passportConfig(passport);

  constructor() {
    this.initializeRoutes();
  }

  private initializeRoutes() {
    this.router.get(`${this.path}/list`, [passport.authenticate('jwt', { session: false })], this.menuController.getAllMenus);

    this.router.post(
      `${this.path}`,
      [passport.authenticate('jwt', { session: false }), [uploadFiles.single('thumbnail')]],
      this.menuController.createMenu,
    );
    this.router.put(
      `${this.path}/:menuId`,
      [passport.authenticate('jwt', { session: false }), [uploadFiles.single('thumbnail')]],
      this.menuController.updateMenu,
    );
    this.router.delete(`${this.path}/:menuId`, [passport.authenticate('jwt', { session: false })], this.menuController.deleteMenu);
  }
}

export default MenuRoute;
