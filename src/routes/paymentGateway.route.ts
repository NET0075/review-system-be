import { Router } from 'express';
import { Routes } from '@interfaces/routes.interface';
import passport from 'passport';
import passportConfig from '@/config/passportConfig';
import validationMiddleware from '@/middlewares/validation.middleware';
import uploadFiles from '@/rest/fileUpload';
import PaymentGatewayController from '@/controllers/paymentGateway.controller';
import { PaymentGateway, PaymentGatewayDto } from '@/dtos/paymentGateway.dto';

class PaymentGatewayRoute implements Routes {
  public path = '/paymentGateway';
  public router = Router();
  public passport = passportConfig(passport);
  public paymentGatewayController = new PaymentGatewayController();

  constructor() {
    this.initializeRoutes();
  }
  private initializeRoutes() {
    this.router.post(
      `${this.path}`,
      passport.authenticate('jwt', { session: false }),
      uploadFiles.single('logo'),
      validationMiddleware(PaymentGateway, 'body'),
      this.paymentGatewayController.createGateway,
    );
    this.router.get(`${this.path}/list`, this.paymentGatewayController.getAllGateway);
    this.router.get(`${this.path}`, this.paymentGatewayController.getAllGateways);

    this.router.get(`${this.path}/:paymentGatewayId`, this.paymentGatewayController.getGatewayById);

    this.router.put(
      `${this.path}/:paymentGatewayId`,
      passport.authenticate('jwt', { session: false }),
      uploadFiles.single('logo'),
      validationMiddleware(PaymentGatewayDto, 'body'),
      this.paymentGatewayController.updateGateway,
    );
    this.router.delete(`${this.path}/:paymentGatewayId`, passport.authenticate('jwt', { session: false }), this.paymentGatewayController.deleteGateway);
  }
}

export default PaymentGatewayRoute;
