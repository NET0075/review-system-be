import { Router } from 'express';
import { Routes } from '@interfaces/routes.interface';
import passport from 'passport';
import passportConfig from '@/config/passportConfig';
import validationMiddleware from '@/middlewares/validation.middleware';
import uploadFiles from '@/rest/fileUpload';
import ReviewPlatformController from '@/controllers/reviewPlatform.controller';
import { ReviewPlatform, ReviewPlatformDto } from '@/dtos/reviewPlatform.dto';

class ReviewPlatformRoute implements Routes {
  public path = '/platform';
  public router = Router();
  public passport = passportConfig(passport);
  public reviewPlatformController = new ReviewPlatformController();

  constructor() {
    this.initializeRoutes();
  }
  private initializeRoutes() {
    this.router.post(
      `${this.path}`,
      passport.authenticate('jwt', { session: false }),
      uploadFiles.single('logo'),
      validationMiddleware(ReviewPlatform, 'body'),
      this.reviewPlatformController.createPlatform,
    );
    this.router.get(`${this.path}/list`, this.reviewPlatformController.getAllPlatform);
    this.router.get(`${this.path}`, this.reviewPlatformController.getAllPlatforms);

    this.router.get(`${this.path}/:platformId`, this.reviewPlatformController.getPlatformById);

    this.router.put(
      `${this.path}/:platformId`,
      passport.authenticate('jwt', { session: false }),
      uploadFiles.single('logo'),
      validationMiddleware(ReviewPlatformDto, 'body'),
      this.reviewPlatformController.updatePlatform,
    );
    this.router.delete(`${this.path}/:platformId`, passport.authenticate('jwt', { session: false }), this.reviewPlatformController.deletePlatform);
  }
}

export default ReviewPlatformRoute;
