import { Router } from 'express';
import { RegisterStaffDto, RegisterUserDto, UpdateStaffDto } from '@dtos/users.dto';
import { Routes } from '@interfaces/routes.interface';
import validationMiddleware from '@middlewares/validation.middleware';
import passport from 'passport';
import passportConfig from '@/config/passportConfig';
import StaffController from '@/controllers/staff.controller';

class StaffRoute implements Routes {
  public path = '/staff';
  public router = Router();
  public staffController = new StaffController();
  public passport = passportConfig(passport);

  constructor() {
    this.initializeRoutes();
  }

  private initializeRoutes() {
    this.router.get(`${this.path}/list`, this.staffController.getAllStaffList);
    this.router.get(`${this.path}/:staffId`, this.staffController.getStaffById);
    this.router.post(`${this.path}`, validationMiddleware(RegisterStaffDto, 'body'), this.staffController.createStaff);
    this.router.put(`${this.path}/:staffId`, validationMiddleware(UpdateStaffDto, 'body'), this.staffController.updateStaff);
    this.router.delete(`${this.path}/:staffId`, validationMiddleware(UpdateStaffDto, 'body'), this.staffController.deleteStaff);
  }
}

export default StaffRoute;
