import { Router } from 'express';
import passport from 'passport';
import { Routes } from '@/interfaces/routes.interface';
import ReviewController from '@/controllers/review.controller';
import validationMiddleware from '@middlewares/validation.middleware';
import { ReviewDTO } from '@/dtos/review.dto';
import passportConfig from '@/config/passportConfig';

class ReviewRoute implements Routes {
  public path = '/review';
  public router = Router();
  public reviewController = new ReviewController();
  public passport = passportConfig(passport);

  constructor() {
    this.initializeRoutes();
  }

  public initializeRoutes() {
    this.router.post(
      `${this.path}`,
      validationMiddleware(ReviewDTO, 'body'),
      passport.authenticate('customer-jwt', { session: false }),
      this.reviewController.createReview,
    );
    this.router.get(`${this.path}/monthWise`, passport.authenticate('jwt', { session: false }), this.reviewController.getAllRestaurantMonthWise);

    this.router.get(`${this.path}/:restaurantId`, passport.authenticate('jwt', { session: false }), this.reviewController.getReview);

    this.router.delete(`${this.path}/:reviewId`, this.reviewController.deleteReview);
    this.router.delete(
      `/admin${this.path}/:reviewId`,
      passport.authenticate('jwt', { session: false }),
      this.reviewController.deleteReviewBySuperAdmin,
    );
  }
}
export default ReviewRoute;
