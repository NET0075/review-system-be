import { Router } from 'express';
import { Routes } from '@interfaces/routes.interface';
import DashboardController from '@/controllers/dashboard.controller';
import passport from 'passport';

class DashBoardRoute implements Routes {
  public path = '/dashboard';
  public router = Router();
  public dashboardController = new DashboardController();
  constructor() {
    this.initializeRoutes();
  }

  private initializeRoutes() {
    this.router.get(`${this.path}/admin/count`, this.dashboardController.getCount);
    this.router.get(`${this.path}/restaurant/count`, passport.authenticate('jwt', { session: false }), this.dashboardController.getCustomerCount);
    this.router.get(
      `${this.path}/admin/monthWise`,
      passport.authenticate('jwt', { session: false }),
      this.dashboardController.getAllRestaurantMonthWise,
    );
  }
}

export default DashBoardRoute;
