import { Router } from 'express';
import { RegisterUserDto } from '@dtos/users.dto';
import { Routes } from '@interfaces/routes.interface';
import validationMiddleware from '@middlewares/validation.middleware';
import passport from 'passport';
import RestaurantController from '@controllers/restaurant.controller';
import passportConfig from '@/config/passportConfig';
import uploadFiles from '@/rest/fileUpload';

class RestaurantRoute implements Routes {
  public path = '/restaurant';
  public router = Router();
  public restaurantController = new RestaurantController();
  public passport = passportConfig(passport);

  constructor() {
    this.initializeRoutes();
  }

  private initializeRoutes() {
    this.router.get(`${this.path}/list`, this.restaurantController.findAllRestaurant);
    this.router.get(`${this.path}`, passport.authenticate('jwt', { session: false }), this.restaurantController.getRestaurantById);
    this.router.get(`${this.path}/:restaurantId`, passport.authenticate('jwt', { session: false }), this.restaurantController.findRestaurantById);
    this.router.post(
      `${this.path}`,
      // validationMiddleware(RegisterUserDto, 'body'),
      uploadFiles.single('logo'),
      this.restaurantController.createRestaurant,
    );
    this.router.put(`${this.path}/:restaurantId`, validationMiddleware(RegisterUserDto, 'body', true), this.restaurantController.updateRestaurant);
    this.router.delete(`${this.path}/:restaurantId`, this.restaurantController.deleteRestaurant);
  }
}

export default RestaurantRoute;
