import { Router } from 'express';
import { Routes } from '@interfaces/routes.interface';
import validationMiddleware from '@middlewares/validation.middleware';
import passportConfig from '@/config/passportConfig';
import passport from 'passport';
import uploadFiles from '@/rest/fileUpload';
import CategoriesController from '@/controllers/menuCategories.controller';
import { MenuCategories } from '@/dtos/menuCategories.dto';

class CategoriesRoute implements Routes {
  public path = '/menu-category';
  public router = Router();
  public menuCategoryController = new CategoriesController();
  public passport = passportConfig(passport);

  constructor() {
    this.initializeRoutes();
  }

  private initializeRoutes() {
    this.router.post(
      `${this.path}`,
      [uploadFiles.single('thumbnail')],
      validationMiddleware(MenuCategories, 'body'),
      this.menuCategoryController.createMenuCategories,
    );
    this.router.get(`${this.path}/list`, this.menuCategoryController.getAllMenuCategories);
    this.router.get(`${this.path}/all`, this.menuCategoryController.getAllMenuCategoriesCustomer);
    this.router.get(
      `${this.path}/:categoryId`,
      [passport.authenticate('jwt', { session: false })],
      this.menuCategoryController.getAllMenuCategoriesById,
    );

    this.router.put(
      `${this.path}/:categoryId`,
      [passport.authenticate('jwt', { session: false }), uploadFiles.single('thumbnail')],
      // validationMiddleware(Categories, 'body'),
      this.menuCategoryController.updateMenuCategories,
    );
    this.router.delete(
      `${this.path}/:categoryId`,
      [passport.authenticate('jwt', { session: false })],
      this.menuCategoryController.deleteMenuCategory,
    );

    this.router.put(
      `${this.path}/toggle-publish/:categoryId`,
      passport.authenticate('jwt', { session: false }),
      this.menuCategoryController.togglePublishCategory,
    );
  }
}

export default CategoriesRoute;
