import { Router } from 'express';
import { Routes } from '@interfaces/routes.interface';
import passport from 'passport';
import CustomerController from '@/controllers/customer.controller';
import passportConfig from '@/config/passportConfig';

class CustomerRoute implements Routes {
  public path = '/customer';
  public router = Router();
  public customerController = new CustomerController();
  public passport = passportConfig(passport);


  constructor() {
    this.initializeRoutes();
  }

  private initializeRoutes() {
    this.router.get(`${this.path}/list`, this.customerController.findAllCustomer);
    this.router.get(`${this.path}/:customerId`, passport.authenticate('jwt', { session: false }), this.customerController.findCustomerById);
    this.router.delete(`${this.path}/:customerId`, this.customerController.deleteCustomer);
  }
}

export default CustomerRoute;
