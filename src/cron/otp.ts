import { schedule } from 'node-cron';
import DB from '@databases';
import { Op } from 'sequelize';

export const deleteExpiredOTPs = () => {
  schedule('*/30 * * * * *', async () => {
    const fiveMinutesAgo = new Date();
    fiveMinutesAgo.setMinutes(fiveMinutesAgo.getMinutes() - 5);

    try {
      const expiredOTPs = await DB.Otp.findAll({
        where: {
          createdAt: {
            [Op.lt]: fiveMinutesAgo, 
          },
        },
      });
      for (const otp of expiredOTPs) {
        await otp.destroy({ force: true });
      }

      // console.log(`Deleted ${expiredOTPs.length} expired OTP records created more than 5 minutes ago.`);
    } catch (error) {
      console.error('Error deleting OTP records:', error);
    }
  });
};
