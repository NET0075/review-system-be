module.exports = (sequelize, Sequelize) => {
  const Menu = sequelize.define(
    'Menu',
    {
      id: {
        type: Sequelize.UUID,
        defaultValue: Sequelize.UUIDV4,
        allowNull: false,
        primaryKey: true,
      },
      name: {
        type: Sequelize.TEXT,
        allowNull: false,
      },
      description: {
        type: Sequelize.STRING,
        allowNull: true,
      },
      price: {
        type: Sequelize.STRING,
        allowNull: true,
      },
      currencySymbol: {
        type: Sequelize.STRING,
        allowNull: true,
      },
      status: {
        type: Sequelize.ENUM(['Active', 'InActive']),
        defaultValue: 'Active',
      },
      isVeg: {
        type: Sequelize.BOOLEAN,
        defaultValue: true,
      },
      isJain: {
        type: Sequelize.BOOLEAN,
        defaultValue: false,
      },
      isSpicy: {
        type: Sequelize.BOOLEAN,
        defaultValue: false,
      },
      isNonVeg: {
        type: Sequelize.BOOLEAN,
        defaultValue: false,
      },
      isHalf: {
        type: Sequelize.BOOLEAN,
        defaultValue: false,
      },
      isEgg: {
        type: Sequelize.BOOLEAN,
        defaultValue: false,
      },
      calories: {
        type: Sequelize.STRING,
        allowNull: true,
      },
      thumbnail: {
        type: Sequelize.TEXT,
      },
    },
    {
      paranoid: true,
    },
  );
  Menu.associate = models => {
    Menu.belongsTo(models.Categories, {
      foreignKey: 'categoryId',
      targetKey: 'id',
    });
  };

  return Menu;
};
