/* eslint-disable @typescript-eslint/no-empty-function */
import { hashSync, genSaltSync, compareSync } from 'bcrypt';
module.exports = (sequelize, Sequelize) => {
  const Review = sequelize.define(
    'Review',
    {
      id: {
        type: Sequelize.UUID,
        defaultValue: Sequelize.UUIDV4,
        allowNull: false,
        primaryKey: true,
      },
      rating: {
        type: Sequelize.FLOAT,
        allowNull: false,
      },
      issueType: {
        type: Sequelize.JSON,
        allowNull: false,
      },
      description: {
        type: Sequelize.TEXT,
        allowNull: false,
      },
      otherDescription: {
        type: Sequelize.TEXT,
        allowNull: true,
      },
    },
    {
      paranoid: true,
    },
  );
  Review.associate = models => {
    Review.belongsTo(models.User, {
      foreignKey: 'userId',
      targetKey: 'id',
    });
    Review.belongsTo(models.Customer, {
      foreignKey: 'customerId',
      targetKey: 'id',
    });
  };
  return Review;
};
