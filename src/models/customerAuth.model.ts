/* eslint-disable @typescript-eslint/no-empty-function */
import { hashSync, genSaltSync, compareSync } from 'bcrypt';
module.exports = (sequelize, Sequelize) => {
  const Customer = sequelize.define(
    'Customer',
    {
      id: {
        type: Sequelize.UUID,
        defaultValue: Sequelize.UUIDV4,
        allowNull: false,
        primaryKey: true,
      },
      firstName: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      lastName: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      number: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      email: {
        type: Sequelize.STRING,
        allowNull: false,
        unique: true,
        validate: {
          isEmail: {
            msg: 'Must be a valid email address',
          },
        },
      },
      dob: {
        type: Sequelize.DATE,
        allowNull: true,
      },
      address: {
        type: Sequelize.TEXT,
        allowNull: true,
      },
    },
    {
      paranoid: true,
    },
  );

  Customer.associate = models => {
    Customer.hasOne(models.Review, {
      foreignKey: 'customerId',
      targetKey: 'id',
    });
  };
  Customer.prototype.validPassword = password => {
    return compareSync(password, Customer.password);
  };

  return Customer;
};
