/* eslint-disable @typescript-eslint/no-empty-function */
import { hashSync, genSaltSync, compareSync } from 'bcrypt';
module.exports = (sequelize, Sequelize) => {
  const Categories = sequelize.define(
    'Categories',
    {
      id: {
        type: Sequelize.UUID,
        defaultValue: Sequelize.UUIDV4,
        allowNull: false,
        primaryKey: true,
      },
      catName: {
        type: Sequelize.TEXT,
        allowNull: false,
      },
      thumbnail: {
        type: Sequelize.STRING,
        allowNull: true,
      },
      status: {
        type: Sequelize.ENUM(['Active', 'InActive']),
        defaultValue: 'Active',
      },
    },
    {
      paranoid: true,
    },
  );
  Categories.associate = models => {
    Categories.belongsTo(models.User, {
      foreignKey: 'userId',
      targetKey: 'id',
    });
    Categories.hasMany(models.Menu, {
      foreignKey: 'categoryId',
      targetKey: 'id',
    });
  };
  return Categories;
};
