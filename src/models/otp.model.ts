/* eslint-disable @typescript-eslint/no-empty-function */
import { hashSync, genSaltSync, compareSync } from 'bcrypt';
import { DataTypes } from 'sequelize';
module.exports = (sequelize, Sequelize) => {
  const Otp = sequelize.define(
    'Otp',
    {
      id: {
        type: Sequelize.UUID,
        defaultValue: Sequelize.UUIDV4,
        allowNull: false,
        primaryKey: true,
      },
      number: {
        allowNull: false,
        type: DataTypes.STRING,
      },
      otp: {
        type: Sequelize.STRING,
        allowNull: false,
      },
    },
    {
      paranoid: true,
    },
  );

  return Otp;
};
