/* eslint-disable @typescript-eslint/no-empty-function */
import { hashSync, genSaltSync, compareSync } from 'bcrypt';
module.exports = (sequelize, Sequelize) => {
  const PaymentGateway = sequelize.define(
    'PaymentGateway',
    {
      id: {
        type: Sequelize.UUID,
        defaultValue: Sequelize.UUIDV4,
        allowNull: false,
        primaryKey: true,
      },
      title: {
        type: Sequelize.TEXT,
      },
      logo: {
        type: Sequelize.STRING,
      },
      fields:{
        type: Sequelize.TEXT,
      }
    },
    {
      paranoid: true,
    },
  );

  PaymentGateway.associate = models => {
    PaymentGateway.hasMany(models.Gateway,{
      foreignKey:"paymentGatewayId",
      targetKey:"id"
    })
  };

  return PaymentGateway;
};
