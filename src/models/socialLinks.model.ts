/* eslint-disable @typescript-eslint/no-empty-function */
import { hashSync, genSaltSync, compareSync } from 'bcrypt';
module.exports = (sequelize, Sequelize) => {
  const SocialLinks = sequelize.define(
    'SocialLinks',
    {
      id: {
        type: Sequelize.UUID,
        defaultValue: Sequelize.UUIDV4,
        allowNull: false,
        primaryKey: true,
      },
      socialLinks: {
        type: Sequelize.TEXT,
        allowNull: false,
      },
      isSelected: {
        type: Sequelize.BOOLEAN,
        defaultValue: false,

      },

    },
    {
      paranoid: true,
    },
  );
  SocialLinks.associate = models => {
    SocialLinks.belongsTo(models.RestaurantProfile, {
      foreignKey: 'restaurantProfileId',
      targetKey: 'id',
    });

    SocialLinks.belongsTo(models.ReviewPlatform, {
      foreignKey: 'platformId',
      targetKey: 'id',
    });

  };
  return SocialLinks;
};
