/* eslint-disable @typescript-eslint/no-empty-function */
import { hashSync, genSaltSync, compareSync } from 'bcrypt';
import { DataTypes } from 'sequelize';
module.exports = (sequelize, Sequelize) => {
  const Role = sequelize.define(
    'Role',
    {
      id: {
        primaryKey: true,
        type: DataTypes.UUID,
        defaultValue: DataTypes.UUIDV4,
      },
      role: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      permission: {
        type: DataTypes.JSON,
        allowNull: false,
        defaultValue: {},
      },
      meta: {
        allowNull: true,
        defaultValue: {},
        type: DataTypes.JSON,
      },
    },
    {
      paranoid: true,
    },
  );

  Role.associate = models => {
    Role.hasMany(models.User, {
      foreignKey: 'roleId',
    });
  };

  return Role;
};
