/* eslint-disable @typescript-eslint/no-empty-function */
import { hashSync, genSaltSync, compareSync } from 'bcrypt';
module.exports = (sequelize, Sequelize) => {
  const ReviewPlatform = sequelize.define(
    'ReviewPlatform',
    {
      id: {
        type: Sequelize.UUID,
        defaultValue: Sequelize.UUIDV4,
        allowNull: false,
        primaryKey: true,
      },
      title: {
        type: Sequelize.TEXT,
      },
      logo: {
        type: Sequelize.STRING,
      },
    },
    {
      paranoid: true,
    },
  );

  ReviewPlatform.associate = models => {
    ReviewPlatform.hasMany(models.SocialLinks,{
      foreignKey:"platformId",
      targetKey:"id"
    })
  };

  return ReviewPlatform;
};
