/* eslint-disable @typescript-eslint/no-empty-function */
import { hashSync, genSaltSync, compareSync } from 'bcrypt';
module.exports = (sequelize, Sequelize) => {
  const User = sequelize.define(
    'User',
    {
      id: {
        type: Sequelize.UUID,
        defaultValue: Sequelize.UUIDV4,
        allowNull: false,
        primaryKey: true,
      },
      fullName: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      email: {
        type: Sequelize.STRING,
        allowNull: false,
        unique: true,
        validate: {
          isEmail: {
            msg: 'Must be a valid email address',
          },
        },
      },
      password: {
        type: Sequelize.STRING,
        set(value) {
          const hashPassword = hashSync(value, genSaltSync(8));
          this.setDataValue('password', hashPassword);
        },
      },

      isApproved: {
        type: Sequelize.ENUM(['Pending', 'Active', 'InActive']),
        defaultValue: 'Pending',
      },
    },
    {
      paranoid: true,
    },
  );

  User.associate = models => {
    User.belongsTo(models.Role, {
      foreignKey: 'roleId',
    });
    User.hasOne(models.RefreshToken, {
      foreignKey: 'userId',
      targetKey: 'id',
    });
    User.hasOne(models.RestaurantProfile, {
      foreignKey: 'userId',
      targetKey: 'id',
    });
    User.hasMany(models.Review, {
      foreignKey: 'userId',
      targetKey: 'id',
    });
  };

  User.prototype.validPassword = password => {
    return compareSync(password, User.password);
  };

  return User;
};
