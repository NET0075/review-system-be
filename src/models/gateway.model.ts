/* eslint-disable @typescript-eslint/no-empty-function */
import { hashSync, genSaltSync, compareSync } from 'bcrypt';
module.exports = (sequelize, Sequelize) => {
  const Gateway = sequelize.define(
    'Gateway',
    {
      id: {
        type: Sequelize.UUID,
        defaultValue: Sequelize.UUIDV4,
        allowNull: false,
        primaryKey: true,
      },
      fields: {
        type: Sequelize.TEXT,
      },
    },
    {
      paranoid: true,
    },
  );
  Gateway.associate = models => {
    Gateway.belongsTo(models.RestaurantProfile, {
      foreignKey: 'restaurantProfileId',
      targetKey: 'id',
    });

    Gateway.belongsTo(models.PaymentGateway, {
      foreignKey: 'paymentGatewayId',
      targetKey: 'id',
    });
  };
  return Gateway;
};
