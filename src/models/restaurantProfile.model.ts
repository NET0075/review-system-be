/* eslint-disable @typescript-eslint/no-empty-function */
import { hashSync, genSaltSync, compareSync } from 'bcrypt';
module.exports = (sequelize, Sequelize) => {
  const RestaurantProfile = sequelize.define(
    'RestaurantProfile',
    {
      id: {
        type: Sequelize.UUID,
        defaultValue: Sequelize.UUIDV4,
        allowNull: false,
        primaryKey: true,
      },
      address1: {
        type: Sequelize.TEXT,
        allowNull: true,
      },
      address2: {
        type: Sequelize.TEXT,
        allowNull: true,
      },
      city: {
        type: Sequelize.STRING,
        allowNull: true,
      },
      state: {
        type: Sequelize.STRING,
        allowNull: true,
      },
      pinCode: {
        type: Sequelize.STRING,
        allowNull: true,
      },
      phoneNo: {
        type: Sequelize.STRING,
        allowNull: true,
      },

      country: {
        type: Sequelize.STRING,
        allowNull: true,
      },
      logo: {
        type: Sequelize.STRING,
        allowNull: true,
      },
      qrCode: {
        type: Sequelize.STRING,
        allowNull: true,
        unique: true,
      },
      isCustomForm: {
        type: Sequelize.BOOLEAN,
        defaultValue: false,
      },
      isProfileUpdated: {
        type: Sequelize.BOOLEAN,
        defaultValue: false,
      },
      latitude: {
        type: Sequelize.STRING,
        allowNull: true,
      },
      longitude: {
        type: Sequelize.STRING,
        allowNull: true,
      },
    },
    {
      paranoid: true,
    },
  );
  RestaurantProfile.associate = models => {
    RestaurantProfile.belongsTo(models.User, {
      foreignKey: 'userId',
      targetKey: 'id',
    });
    RestaurantProfile.hasMany(models.SocialLinks, {
      foreignKey: 'restaurantProfileId',
      targetKey: 'id',
    });
    RestaurantProfile.hasMany(models.Gateway, {
      foreignKey: 'restaurantProfileId',
      targetKey: 'id',
    });

    // RestaurantProfile.hasMany(models.ReviewPlatform, {
    //   foreignKey: 'profileId',
    //   targetKey: 'id',
    // });
  };

  return RestaurantProfile;
};
