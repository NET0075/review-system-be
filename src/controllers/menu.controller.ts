import { NextFunction, Request, Response } from 'express';
import MenuService from '@/services/menu.service';

class MenuController {
  public menu = new MenuService();

  public getAllMenus = async (req: Request, res: Response, next: NextFunction) => {
    try {
      const { search, pageRecord, pageNo, sortBy, order, isApproved, menuCategoryId } = req.query;
      const queryObject = {
        search,
        pageRecord,
        pageNo,
        sortBy,
        order,
        isApproved,
        menuCategoryId,
      };
      const response = await this.menu.getAllMenus(queryObject);
      res.status(200).send(response);
    } catch (error) {
      next(error);
    }
  };

  public createMenu = async (req: Request, res: Response, next: NextFunction) => {
    try {
      const user = req.user;
      const menuData = req.body;
      const file = req.file;
      const response = await this.menu.createMenu({ user, menuData, file });
      res.status(200).send({ response: response, message: 'Menu added Successfully' });
    } catch (error) {
      next(error);
    }
  };
  public updateMenu = async (req: Request, res: Response, next: NextFunction) => {
    try {
      const user = req.user;
      const menuData = req.body;
      const { menuId } = req.params;
      const file = req.file;
      const response = await this.menu.updateMenu({ user, menuData, menuId, file });
      res.status(200).send({ response: response, message: 'Menu updated Successfully' });
    } catch (error) {
      next(error);
    }
  };

  public deleteMenu = async (req: Request, res: Response, next: NextFunction) => {
    try {
      const user = req.user;
      const { menuId } = req.params;
      const response = await this.menu.deleteMenu({ user, menuId });
      res.status(200).send({ response: response, message: 'Menu deleted Successfully' });
    } catch (error) {
      next(error);
    }
  };
}

export default MenuController;
