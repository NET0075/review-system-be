import { NextFunction, Request, Response } from 'express';
import DashboardService from '@/services/dashboard.service';
import { RequestWithUser } from '@/interfaces/auth.interface';

class DashboardController {
  public dashboardService = new DashboardService();

  public getCount = async (req: Request, res: Response, next: NextFunction) => {
    try {
      const findAllUsersData: { restaurantCount: number; customerCount: number } = await this.dashboardService.getCount();
      res.status(200).send(findAllUsersData);
    } catch (error) {
      next(error);
    }
  };
  public getCustomerCount = async (req: RequestWithUser, res: Response, next: NextFunction) => {
    try {
      const user = req.user;
      const findAllReviewData: { reviewCount: number } = await this.dashboardService.getCustomerCount({ user });
      res.status(200).send(findAllReviewData);
    } catch (error) {
      next(error);
    }
  };
  public getAllRestaurantMonthWise = async (req: RequestWithUser, res: Response, next: NextFunction) => {
    try {
      const user = req.user;
      const response = await this.dashboardService.getAllRestaurantMonthWise(user);
      res.status(200).send(response);
    } catch (err) {
      next(err);
    }
  };
}

export default DashboardController;
