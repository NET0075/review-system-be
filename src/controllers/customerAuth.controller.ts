import { NextFunction, Request, Response } from 'express';
import CustomerAuthService from '@services/customerAuth.service';
import TokenService from '@/services/token.service';
import { LoginCustomerDTO, RegisterCustomerDto } from '@/dtos/customer.dto';
// import { sendVerification } from '@/config/twilio';

class CustomerAuthController {
  public customerAuthService = new CustomerAuthService();
  public tokenService = new TokenService();

  public signUp = async (req: Request, res: Response, next: NextFunction) => {
    try {
      const customerData: RegisterCustomerDto = req.body;
      const { accessToken, refreshToken, customer } = await this.customerAuthService.signup(customerData);
      res.status(200).send({
        accessToken,
        refreshToken,
        customer,
        message: 'Signed up successfully',
      });
    } catch (error) {
      next(error);
    }
  };

  public logIn = async (req: Request, res: Response, next: NextFunction) => {
    try {
      const { mobileNumber }: LoginCustomerDTO = req.body;
      const { otp } = await this.customerAuthService.login({ mobileNumber });
      // sendVerification(mobileNumber, otp);
      res.status(200).send({ otp, message: 'Otp sent successfully and valid for 5 minutes' });
    } catch (error) {
      next(error);
    }
  };

  public validateOtp = async (req: Request, res: Response, next: NextFunction) => {
    try {
      const { mobileNumber, otp } = req.body;
      const data = await this.customerAuthService.validateOtp({ mobileNumber, otp });
      res.status(200).send(data);
    } catch (error) {
      next(error);
    }
  };
}

export default CustomerAuthController;
