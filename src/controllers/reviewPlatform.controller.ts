import { NextFunction, Request, Response } from 'express';
import ReviewPlatformService from '@/services/reviewPlatform.service';

class ReviewPlatformController {
  public reviewPlatformService = new ReviewPlatformService();

  public createPlatform = async (req: Request, res: Response, next: NextFunction) => {
    try {
      const user = req.user;
      const file = req.file;
      const platformData = req.body;
      const response = await this.reviewPlatformService.createReviewPlatform({
        platformData,
        file,
        user,
      });
      res.status(200).send({ response: response, messages: 'Platform created successfully' });
    } catch (err) {
      next(err);
    }
  };

  public getAllPlatform = async (req: Request, res: Response, next: NextFunction) => {
    try {
      const { search, pageRecord, pageNo, sortBy, order } = req.query;
      const queryObject = { search, pageRecord, pageNo, sortBy, order };
      const response: { totalCount: number; records: undefined } = await this.reviewPlatformService.getAllPlatform({ queryObject });
      res.status(200).send(response);
    } catch (err) {
      next(err);
    }
  };

  public getAllPlatforms = async (req: Request, res: Response, next: NextFunction) => {
    try {
      const response: { totalCount: number; records: undefined } = await this.reviewPlatformService.getAllPlatforms();
      res.status(200).send(response);
    } catch (err) {
      next(err);
    }
  };
  public getPlatformById = async (req: Request, res: Response, next: NextFunction) => {
    try {
      const { platformId } = req.params;
      const response = await this.reviewPlatformService.getPlatformById({ platformId });
      res.status(200).send(response);
    } catch (err) {
      next(err);
    }
  };

  public updatePlatform = async (req: Request, res: Response, next: NextFunction) => {
    try {
      const user = req.user;
      const file = req.file;
      const platformData = req.body;
      const { platformId } = req.params;
      const response = await this.reviewPlatformService.updatePlatform({
        platformData,
        file,
        user,
        platformId,
      });
      res.status(200).send({ response: response, message: 'Platform updated successfully' });
    } catch (err) {
      next(err);
    }
  };

  public deletePlatform = async (req: Request, res: Response, next: NextFunction) => {
    try {
      const user = req.user;
      const { platformId } = req.params;
      const response = await this.reviewPlatformService.deletePlatform({
        user,
        platformId,
      });
      res.status(200).send({ response: response, message: 'Platform deleted successfully' });
    } catch (err) {
      next(err);
    }
  };
}
export default ReviewPlatformController;
