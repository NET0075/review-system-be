import { NextFunction, Response } from 'express';
import RestaurantProfileService from '@/services/restaurantProfile.service';
import { RequestWithUser } from '@/interfaces/auth.interface';

class RestaurantProfileController {
  public restaurantService = new RestaurantProfileService();

  public createRestaurantProfile = async (req: RequestWithUser, res: Response, next: NextFunction) => {
    try {
      const user = req.user;
      const profileData = req.body;
      const file = req.file;
      const response = await this.restaurantService.createRestaurantProfile({ user, profileData, file });
      res.status(200).send({ response: response, message: 'Profile created Successfully' });
    } catch (error) {
      next(error);
    }
  };

  public getRestaurantProfile = async (req: RequestWithUser, res: Response, next: NextFunction) => {
    try {
      const user = req.user;
      const response = await this.restaurantService.getRestaurantProfile({ user });
      res.status(200).send(response);
    } catch (error) {
      next(error);
    }
  };

  public updateRestaurantProfileById = async (req: RequestWithUser, res: Response, next: NextFunction) => {
    try {
      const { restaurantId } = req.params;
      const profileData = req.body;
      const file = req.file;
      const response = await this.restaurantService.updateRestaurantProfileById({ restaurantId, profileData, file });
      res.status(200).send({ response: response, message: 'Profile updated Successfully' });
    } catch (error) {
      next(error);
    }
  };

  public updateRestaurantProfile = async (req: RequestWithUser, res: Response, next: NextFunction) => {
    try {
      const user = req.user;
      const profileData = req.body;
      const file = req.file;
      const response = await this.restaurantService.updateRestaurantProfile({ user, profileData, file });
      res.status(200).send({ response: response, message: 'Profile updated Successfully' });
    } catch (error) {
      next(error);
    }
  };

  public deleteRestaurantProfile = async (req: RequestWithUser, res: Response, next: NextFunction) => {
    try {
      const user = req.user;
      const response = await this.restaurantService.deleteRestaurantProfile({ user });
      res.status(200).send({ response: response, message: 'Profile deleted Successfully' });
    } catch (error) {
      next(error);
    }
  };
  public deleteSocialLinks = async (req: RequestWithUser, res: Response, next: NextFunction) => {
    try {
      const user = req.user;
      const { socialLinkId } = req.params;
      const response = await this.restaurantService.deleteSocialLinks({ socialLinkId, user });
      res.status(200).send({ response: response, message: 'Social links deleted Successfully' });
    } catch (error) {
      next(error);
    }
  };
  public getRestaurantByQRCode = async (req: RequestWithUser, res: Response, next: NextFunction) => {
    try {
      const user = req.user;
      const { qrCode, latitude, longitude } = req.params;
      const response = await this.restaurantService.getRestaurantByQRCode({ qrCode, user, latitude, longitude });
      res.status(200).send(response);
    } catch (error) {
      next(error);
    }
  };
}

export default RestaurantProfileController;
