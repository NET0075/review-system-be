import { NextFunction, Request, Response } from 'express';
import StaffService from '@/services/staff.service';

class StaffController {
  public staffservice = new StaffService();

  public getAllStaffList = async (req: Request, res: Response, next: NextFunction) => {
    try {
      const { search, pageRecord, pageNo, sortBy, order } = req.query;
      const queryObject = { search, pageRecord, pageNo, sortBy, order };
      const response: { totalCount: number; records: undefined } = await this.staffservice.getAllStaffList(queryObject);
      res.status(200).send(response);
    } catch (error) {
      next(error);
    }
  };

  public getStaffById = async (req: Request, res: Response, next: NextFunction) => {
    try {
      const { staffId } = req.params;
      const response = await this.staffservice.getStaffById(staffId);
      res.status(200).send(response);
    } catch (error) {
      next(error);
    }
  };

  public createStaff = async (req: Request, res: Response, next: NextFunction) => {
    try {
      const staffData = req.body;
      const createUserData = await this.staffservice.createStaff({ staffData });
      res.status(200).send({ data: createUserData, message: 'Staff created Successfully' });
    } catch (error) {
      next(error);
    }
  };

  public updateStaff = async (req: Request, res: Response, next: NextFunction) => {
    try {
      const { staffId } = req.params;
      const staffData = req.body;
      const createUserData = await this.staffservice.updateStaff({ staffData, staffId });
      res.status(200).send({ data: createUserData, message: 'Staff updated Successfully' });
    } catch (error) {
      next(error);
    }
  };

  public deleteStaff = async (req: Request, res: Response, next: NextFunction) => {
    try {
      const { staffId } = req.params;
      const createUserData = await this.staffservice.deleteStaff(staffId);
      res.status(200).send({ data: createUserData, message: 'Staff deleted Successfully' });
    } catch (error) {
      next(error);
    }
  };
}

export default StaffController;
