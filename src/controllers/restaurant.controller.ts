import { NextFunction, Request, Response } from 'express';
import { User } from '@interfaces/users.interface';
import RestaurantService from '@/services/restaurant.service';
import { RequestWithUser } from '@/interfaces/auth.interface';

class RestaurantController {
  public restaurantService = new RestaurantService();

  public findAllRestaurant = async (req: Request, res: Response, next: NextFunction) => {
    try {
      const { search, pageRecord, pageNo, sortBy, order, categoryId, isApproved } = req.query;
      const queryObject = {
        search,
        pageRecord,
        pageNo,
        sortBy,
        order,
        categoryId,
        isApproved,
      };
      const findAllUsersData = await this.restaurantService.findAllRestaurant(queryObject);
      res.status(200).send(findAllUsersData);
    } catch (error) {
      next(error);
    }
  };

  public getRestaurantById = async (req: RequestWithUser, res: Response, next: NextFunction) => {
    try {
      const { id: restaurantId } = req.user;
      const findOneUserData = await this.restaurantService.getRestaurantById(restaurantId);
      res.status(200).send(findOneUserData);
    } catch (error) {
      next(error);
    }
  };

  public findRestaurantById = async (req: RequestWithUser, res: Response, next: NextFunction) => {
    try {
      const { restaurantId } = req.params;
      const findOneUserData = await this.restaurantService.findRestaurantById(restaurantId);
      res.status(200).send(findOneUserData);
    } catch (error) {
      next(error);
    }
  };

  public createRestaurant = async (req: Request, res: Response, next: NextFunction) => {
    try {
      const restaurantData = req.body;
      const file = req.file;
      const createUserData = await this.restaurantService.createRestaurant({ restaurantData, file });
      res.status(200).send({ data: createUserData, message: 'Restaurant created Successfully' });
    } catch (error) {
      next(error);
    }
  };

  public updateRestaurant = async (req: Request, res: Response, next: NextFunction) => {
    try {
      const { restaurantId } = req.params;
      const restaurantData = req.body;
      const updateUserData: User = await this.restaurantService.updateRestaurant(restaurantId, restaurantData);
      res.status(200).send({ data: updateUserData, message: 'Restaurant updated Successfully' });
    } catch (error) {
      next(error);
    }
  };

  public deleteRestaurant = async (req: Request, res: Response, next: NextFunction) => {
    try {
      const { restaurantId } = req.params;
      const deleteUserData: User = await this.restaurantService.deleteRestaurant(restaurantId);
      res.status(200).send({ data: deleteUserData, message: 'Restaurant deleted Successfully' });
    } catch (error) {
      next(error);
    }
  };

}

export default RestaurantController;
