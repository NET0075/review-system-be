import { NextFunction, Request, Response } from 'express';
import CategoriesService from '@/services/menuCategories.service';

class CategoriesController {
  public menuCategories = new CategoriesService();

  public createMenuCategories = async (req: Request, res: Response, next: NextFunction) => {
    try {
      const user = req.user;
      const categoryData = req.body;
      const file = req.file;
      const response = await this.menuCategories.createMenuCategories({ user, categoryData, file });
      res.status(200).send({ response: response, message: 'Category created Successfully' });
    } catch (error) {
      next(error);
    }
  };

  public getAllMenuCategories = async (req: Request, res: Response, next: NextFunction) => {
    try {
      const { search, pageRecord, pageNo, sortBy, order, isApproved, userId } = req.query;
      const queryObject = {
        search,
        pageRecord,
        pageNo,
        sortBy,
        order,
        isApproved,
        userId,
      };
      const response = await this.menuCategories.getAllMenuCategories(queryObject);
      res.status(200).send(response);
    } catch (error) {
      next(error);
    }
  };

  public getAllMenuCategoriesCustomer = async (req: Request, res: Response, next: NextFunction) => {
    try {
      const { userId } = req.query;
      const queryObject = {
        userId,
      };
      const response = await this.menuCategories.getAllMenuCategoriesCustomer(queryObject);
      res.status(200).send(response);
    } catch (error) {
      next(error);
    }
  };

  public getAllMenuCategoriesById = async (req: Request, res: Response, next: NextFunction) => {
    try {
      const { search, pageRecord, pageNo, sortBy, order } = req.query;
      const queryObject = {
        search,
        pageRecord,
        pageNo,
        sortBy,
        order,
      };
      const { categoryId } = req.params;
      const response = await this.menuCategories.getAllMenuCategoriesById({ queryObject, categoryId });
      res.status(200).send(response);
    } catch (error) {
      next(error);
    }
  };

  public updateMenuCategories = async (req: Request, res: Response, next: NextFunction) => {
    try {
      const user = req.user;
      const categoryData = req.body;
      const file = req.file;
      const { categoryId } = req.params;
      const response = await this.menuCategories.updateMenuCategories({ user, categoryData, file, categoryId });
      res.status(200).send({ response: response, message: 'Category updated Successfully' });
    } catch (error) {
      next(error);
    }
  };

  public deleteMenuCategory = async (req: Request, res: Response, next: NextFunction) => {
    try {
      const user = req.user;
      const { categoryId } = req.params;
      const response = await this.menuCategories.deleteMenuCategory({ user, categoryId });
      res.status(200).send({ response: response, message: 'Category deleted Successfully \uD83D\uDE0A' });
    } catch (error) {
      next(error);
    }
  };

  public togglePublishCategory = async (req: Request, res: Response, next: NextFunction) => {
    try {
      const { categoryId } = req.params;
      const trainer = req.user;
      const response: { count: number } = await this.menuCategories.togglePublishCategory({
        trainer,
        categoryId,
      });
      res.status(200).send(response);
    } catch (error) {
      next(error);
    }
  };
}

export default CategoriesController;
