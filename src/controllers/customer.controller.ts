import { NextFunction, Request, Response } from 'express';
import { User } from '@interfaces/users.interface';
import CustomerService from '@/services/customer.service';

class CustomerController {
  public customerService = new CustomerService();

  public findAllCustomer = async (req: Request, res: Response, next: NextFunction) => {
    try {
      const { search, pageRecord, pageNo, sortBy, order, categoryId, isApproved } = req.query;
      const queryObject = {
        search,
        pageRecord,
        pageNo,
        sortBy,
        order,
        categoryId,
        isApproved,
      };
      const findAllCustomerData: { totalCount: number; records: User | undefined } = await this.customerService.findAllCustomer(queryObject);
      res.status(200).send(findAllCustomerData);
    } catch (error) {
      next(error);
    }
  };

  public findCustomerById = async (req: Request, res: Response, next: NextFunction) => {
    try {
      const { customerId } = req.params as any;
      const findOneUserData = await this.customerService.findCustomerById(customerId);
      res.status(200).send(findOneUserData);
    } catch (error) {
      next(error);
    }
  };

  public deleteCustomer = async (req: Request, res: Response, next: NextFunction) => {
    try {
      const { customerId } = req.params;
      const deleteUserData: User = await this.customerService.deleteCustomer(customerId);
      res.status(200).send({ data: deleteUserData, message: 'Customer deleted Successfully' });
    } catch (error) {
      next(error);
    }
  };
}

export default CustomerController;
