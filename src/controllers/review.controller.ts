import { Review } from '@/interfaces/review.interface';
import ReviewService from '@/services/review.service';
import { NextFunction, Request, Response } from 'express';

class ReviewController {
  public reviewSevice = new ReviewService();

  public createReview = async (req: Request, res: Response, next: NextFunction) => {
    try {
      const reviewDetail = req.body;
      const customer = req.user;
      const response = await this.reviewSevice.createReview({ reviewDetail, customer });
      res.status(200).send({ response: response, message: 'Review added Successfully' });
    } catch (error) {
      next(error);
    }
  };
  public getReview = async (req: Request, res: Response, next: NextFunction) => {
    try {
      const { restaurantId } = req.params;
      const { search, pageRecord, pageNo, sortBy, order } = req.query;
      const queryObject = { search, pageRecord, pageNo, sortBy, order };
      const response: {
        totalCount: number;
        records: (Review | undefined)[];
      } = await this.reviewSevice.getReview({ queryObject, restaurantId });
      res.status(200).send(response);
    } catch (error) {
      next(error);
    }
  };

  public deleteReview = async (req: Request, res: Response, next: NextFunction) => {
    try {
      const { reviewId } = req.params;
      const response: { count: number } = await this.reviewSevice.deleteReview(reviewId);
      res.status(200).send({ response: response, message: 'Review Deleted Successfully' });
    } catch (error) {
      next(error);
    }
  };

  public deleteReviewBySuperAdmin = async (req: Request, res: Response, next: NextFunction) => {
    try {
      const { reviewId } = req.params;
      const user = req.user;
      const response: { count: number } = await this.reviewSevice.deleteReviewBySuperAdmin({ reviewId, user });
      res.status(200).send({ response: response, message: 'Review Deleted Successfully' });
    } catch (error) {
      next(error);
    }
  };

  public getAllRestaurantMonthWise = async (req: Request, res: Response, next: NextFunction) => {
    try {
      const restaurant = req.user;
      const response = await this.reviewSevice.getAllReviewMonthWise(restaurant);
      res.status(200).send(response);
    } catch (err) {
      next(err);
    }
  };
}
export default ReviewController;
