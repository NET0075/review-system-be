import { NextFunction, Request, Response } from 'express';
import { RegisterUserDto, ResetPasswordDTO } from '@dtos/users.dto';
import { RefreshToken } from '@/interfaces/refreshToken.interface';
import AuthService from '@services/auth.service';
import TokenService from '@/services/token.service';

class AuthController {
  public authService = new AuthService();
  public tokenService = new TokenService();

  public signUp = async (req: Request, res: Response, next: NextFunction) => {
    try {
      const userData: RegisterUserDto = req.body;
      const { accessToken, refreshToken, user } = await this.authService.signup(userData);
      res.status(200).send({
        accessToken,
        refreshToken,
        user,
        message: 'Signed up successfully',
      });
    } catch (error) {
      next(error);
    }
  };
  public logIn = async (req: Request, res: Response, next: NextFunction) => {
    try {
      const { email, password, cookie } = req.body;
      const { refreshToken, accessToken, user } = await this.authService.login({
        email,
        password,
      });

      res.status(200).send({
        accessToken,
        refreshToken,
        user,
        message: `Logged in Successfully`,
      });
    } catch (error) {
      next(error);
    }
  };
  public refreshTokenUser = async (req: Request, res: Response, next: NextFunction) => {
    try {
      const { refreshToken: requestToken } = req.body;

      if (requestToken === null) {
        return res.status(403).send({
          redirectToLogin: true,
          message: 'Refresh Token is required!',
        });
      }
      let refreshTokenData: RefreshToken = await this.tokenService.findToken(requestToken);
      if (!refreshTokenData)
        res.status(403).send({
          message: 'Invalid Token!',
        });
      let refreshTokenInvalid = await this.tokenService.verifyToken(refreshTokenData);
      if (refreshTokenInvalid) {
        res.status(403).send({
          message: 'Expired Token!',
        });
        return;
      }
      const { accessToken, refreshToken, user } = await this.tokenService.refreshTokenUser(refreshTokenData);

      return res.status(200).send({ accessToken, refreshToken, user });
    } catch (error) {
      next(error);
    }
  };

  public forgotPassword = async (req: Request, res: Response, next: NextFunction) => {
    try {
      const { email } = req.body;
      const response = await this.authService.forgotPassword(email);
      res.status(200).json({ data: response, status: 'Success' });
    } catch (error) {
      next(error);
    }
  };
  public verifyPasswordToken = async (req: Request, res: Response, next: NextFunction) => {
    try {
      const { token } = req.params;
      const response = await this.authService.verifyPasswordtoken(token);
      res.status(200).send(response);
    } catch (error) {
      next(error);
    }
  };
  public resetPassword = async (req: Request, res: Response, next: NextFunction) => {
    try {
      const { token, newPassword }: ResetPasswordDTO = req.body;
      const response = await this.authService.resetPassword(token, newPassword);
      res.status(200).send(response);
    } catch (error) {
      next(error);
    }
  };
  public togglePublishRestaurant = async (req: Request, res: Response, next: NextFunction) => {
    try {
      const { restaurantId } = req.params;
      const trainer = req.user;
      const response: { count: number } = await this.authService.togglePublishRestaurant({
        trainer,
        restaurantId,
      });
      res.status(200).send(response);
    } catch (error) {
      next(error);
    }
  };
}

export default AuthController;
