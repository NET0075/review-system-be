import { NextFunction, Request, Response } from 'express';
import PaymentGatewayService from '@/services/paymentGateway.service';

class PaymentGatewayController {
  public paymentGatewayService = new PaymentGatewayService();

  public createGateway = async (req: Request, res: Response, next: NextFunction) => {
    try {
      const user = req.user;
      const file = req.file;
      const gatewayData = req.body;
      const response = await this.paymentGatewayService.createPaymentGateway({
        gatewayData,
        file,
        user,
      });
      res.status(200).send({ response: response, messages: 'Gateway created successfully' });
    } catch (err) {
      next(err);
    }
  };

  public getAllGateway = async (req: Request, res: Response, next: NextFunction) => {
    try {
      const { search, pageRecord, pageNo, sortBy, order } = req.query;
      const queryObject = { search, pageRecord, pageNo, sortBy, order };
      const response: { totalCount: number; records: undefined } = await this.paymentGatewayService.getAllGateway({ queryObject });
      res.status(200).send(response);
    } catch (err) {
      next(err);
    }
  };

  public getAllGateways = async (req: Request, res: Response, next: NextFunction) => {
    try {
      const response: { totalCount: number; records: undefined } = await this.paymentGatewayService.getAllGateways();
      res.status(200).send(response);
    } catch (err) {
      next(err);
    }
  };
  public getGatewayById = async (req: Request, res: Response, next: NextFunction) => {
    try {
      const { paymentGatewayId } = req.params;
      const response = await this.paymentGatewayService.getGatewayById({ paymentGatewayId });
      res.status(200).send(response);
    } catch (err) {
      next(err);
    }
  };

  public updateGateway = async (req: Request, res: Response, next: NextFunction) => {
    try {
      const user = req.user;
      const file = req.file;
      const gatewayData = req.body;
      const { paymentGatewayId } = req.params;
      const response = await this.paymentGatewayService.updateGateway({
        gatewayData,
        file,
        user,
        paymentGatewayId,
      });
      res.status(200).send({ response: response, message: 'Gateway updated successfully' });
    } catch (err) {
      next(err);
    }
  };

  public deleteGateway = async (req: Request, res: Response, next: NextFunction) => {
    try {
      const user = req.user;
      const { paymentGatewayId } = req.params;
      const response = await this.paymentGatewayService.deleteGateway({
        user,
        paymentGatewayId,
      });
      res.status(200).send({ response: response, message: 'Gateway deleted successfully' });
    } catch (err) {
      next(err);
    }
  };
}
export default PaymentGatewayController;
