import { NextFunction, Request, Response } from 'express';
import RoleService from '@/services/role.service';

class RoleController {
  public roleService = new RoleService();

  public getAllRole = async (req: Request, res: Response, next: NextFunction) => {
    try {
      const { search, pageRecord, pageNo, sortBy, order } = req.query;
      const queryObject = { search, pageRecord, pageNo, sortBy, order };
      const response: {
        totalCount: number;
        records: undefined[];
      } = await this.roleService.getAllRole(queryObject);
      res.status(200).send(response);
    } catch (error) {
      next(error);
    }
  };
  public findAllRoles = async (req: Request, res: Response, next: NextFunction) => {
    try {
      const response = await this.roleService.findAllRoles();
      res.status(200).send(response);
    } catch (error) {
      next(error);
    }
  };

  public getRoleById = async (req: Request, res: Response, next: NextFunction) => {
    try {
      const { roleId } = req.params;
      const response = await this.roleService.getRolebyId(roleId);
      res.status(200).send(response);
    } catch (error) {
      next(error);
    }
  };

  public createRole = async (req: Request, res: Response, next: NextFunction) => {
    try {
      const user = req.user;
      const roleData = req.body;
      const response = await this.roleService.createRole({ user, roleData });
      res.status(200).send({ response: response, message: 'Role created Successfully' });
    } catch (error) {
      next(error);
    }
  };
  public updateRole = async (req: Request, res: Response, next: NextFunction) => {
    try {
      const user = req.user;
      const { roleId } = req.params;
      const roleData = req.body;
      const response = await this.roleService.updateRole({ roleId, roleData });
      res.status(200).send({ response: response, message: 'Role updated Successfully' });
    } catch (error) {
      next(error);
    }
  };
  public deleteRole = async (req: Request, res: Response, next: NextFunction) => {
    try {
      const { roleId } = req.params;
      const response = await this.roleService.deleteRole({ roleId });
      res.status(200).send({ response: response, message: 'Role deleted Successfully' });
    } catch (error) {
      next(error);
    }
  };
}

export default RoleController;
