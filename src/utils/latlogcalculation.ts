import { CONSTANTS } from '../constants/index';

export type Coordinates = {
  longitude: number;
  latitude: number;
};

const degreesToRadians = (degrees: number) => {
  const radians = (degrees * Math.PI) / 180;
  return radians;
};

const calcCoordsDistance = (starting: Coordinates, destination: Coordinates): number => {
  const startingLat = degreesToRadians(starting.latitude);
  const startingLong = degreesToRadians(starting.longitude);
  const destinationLat = degreesToRadians(destination.latitude);
  const destinationLong = degreesToRadians(destination.longitude);

  // Radius of the Earth in kilometers
  const radius = 6371;

  // Haversine equation
  const distanceInKilometers: number =
    Math.acos(
      Math.sin(startingLat) * Math.sin(destinationLat) + Math.cos(startingLat) * Math.cos(destinationLat) * Math.cos(startingLong - destinationLong),
    ) * radius;
  const resultToMiles = distanceInKilometers / CONSTANTS.KM_TO_MILES;
  const result = Math.floor(resultToMiles * 100) / 100;
  return result;
};

export default calcCoordsDistance;
