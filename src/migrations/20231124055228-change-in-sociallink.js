'use strict';

/** @type {import('sequelize').Sequelize } */
module.exports = {
  up: async (queryInterface, Sequelize) => {
    // Step 1: Change the data type of the 'socialLinks' field to TEXT
    await queryInterface.changeColumn('SocialLinks', 'social_links', {
      type: Sequelize.TEXT,
      allowNull: false,
    });
  },

  down: async (queryInterface, Sequelize) => {
    // Step 1: Revert the changes made in 'up' function
    await queryInterface.changeColumn('SocialLinks', 'social_links', {
      type: Sequelize.STRING, // Revert back to the original data type
      allowNull: false,
    });
  },
};
