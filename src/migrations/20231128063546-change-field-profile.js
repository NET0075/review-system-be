'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    // Step 1: Rename the 'address' field to 'address1'
    await queryInterface.renameColumn('RestaurantProfile', 'address', 'address1');

    // Step 2: Add a new field 'address2'
    await queryInterface.addColumn('RestaurantProfile', 'address2', {
      type: Sequelize.TEXT,
      allowNull: true,
    });
  },

  down: async (queryInterface, Sequelize) => {
    // Step 1: Revert the changes made in 'up' function

    // Revert adding 'address2'
    await queryInterface.removeColumn('RestaurantProfile', 'address2');

    // Step 2: Rename 'address1' back to 'address'
    await queryInterface.renameColumn('RestaurantProfile', 'address1', 'address');
  },
};
