'use strict';

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.addColumn('RestaurantProfile', 'latitude', {
      type: Sequelize.STRING,
      allowNull: true,
    });
    await queryInterface.addColumn('RestaurantProfile', 'longitude', {
      type: Sequelize.STRING,
      allowNull: true,
    });
  },

  async down(queryInterface, Sequelize) {
    await queryInterface.removeColumn('RestaurantProfile', 'latitude');
    await queryInterface.removeColumn('RestaurantProfile', 'longitude');

  },
};
