import { IsArray, IsEnum, IsNumber, IsOptional, IsString, Max } from 'class-validator';

export class ReviewDTO {
  @IsNumber()
  @Max(5)
  public rating: number;

  @IsString()
  public restaurantId: string;

  @IsString()
  public description: string;

  @IsArray()
  @IsString({ each: true })
  public issueType: string[];

  @IsString()
  @IsOptional()
  public otherDescription: string;
}
