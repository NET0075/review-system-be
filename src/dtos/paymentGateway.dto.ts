import { IsArray, IsEnum, IsNumber, IsOptional, IsString, Max } from 'class-validator';

export class PaymentGateway {
  @IsString()
  public title: string;

  @IsString()
  public fields: string;

}

export class PaymentGatewayDto {
  @IsString()
  @IsOptional()
  public title: string;

  @IsString()
  @IsOptional()
  public logo: string;

  @IsString()
  @IsOptional()
  public fields: string;
}
