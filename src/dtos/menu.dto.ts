import { IsBoolean, IsEnum, IsNumber, IsOptional, IsString, Max, Min } from 'class-validator';

export class MenuDto {
  @IsNumber()
  @Min(1)
  public price: number;

  @IsString()
  public name: string;

  @IsString()
  public description: string;

  @IsBoolean()
  @IsOptional()
  public isVeg: boolean;

  @IsBoolean()
  @IsOptional()
  public isJain: boolean;

  @IsBoolean()
  @IsOptional()
  public isSpicy: boolean;

  @IsBoolean()
  @IsOptional()
  public isNonVeg: boolean;

  @IsBoolean()
  @IsOptional()
  public isHalf: boolean;

  @IsBoolean()
  @IsOptional()
  public isEgg: boolean;

  @IsString()
  public categoryId: string;

  @IsNumber()
  @IsOptional()
  public calories: number;
}

export class MenuDTO {
  @IsNumber()
  @Min(1)
  @IsOptional()
  public price: number;

  @IsString()
  @IsOptional()
  public name: string;

  @IsString()
  @IsOptional()
  public description: string;

  @IsBoolean()
  @IsOptional()
  public isVeg: boolean;

  @IsBoolean()
  @IsOptional()
  public isJain: boolean;

  @IsBoolean()
  @IsOptional()
  public isSpicy: boolean;

  @IsBoolean()
  @IsOptional()
  public isNonVeg: boolean;

  @IsBoolean()
  @IsOptional()
  public isHalf: boolean;

  @IsBoolean()
  @IsOptional()
  public isEgg: boolean;

  @IsString()
  @IsOptional()
  public categoryId: boolean;

  @IsNumber()
  @IsOptional()
  public calories: number;
}
