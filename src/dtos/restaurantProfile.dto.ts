import { IsArray, IsBoolean, IsEnum, IsNumber, IsOptional, IsString, Max } from 'class-validator';

export class RestaurantProifle {
  @IsString()
  public address: string;

  @IsString()
  public pinCode: string;

  @IsString()
  public city: string;

  @IsString()
  public state: string;

  @IsString()
  public phoneNo: string;

  @IsString()
  public country: string;

  @IsArray()
  @IsOptional()
  public socialLinks: string;

  @IsString()
  public platformId: string;
}

export class RestaurantProifleDto {
  @IsString()
  @IsOptional()
  public address: string;

  @IsString()
  @IsOptional()
  public pinCode: string;

  @IsString()
  @IsOptional()
  public city: string;

  @IsString()
  @IsOptional()
  public state: string;

  @IsString()
  @IsOptional()
  public phoneNo: string;

  @IsString()
  @IsOptional()
  public country: string;

  @IsString()
  @IsOptional()
  public fullName: string;

  @IsString()
  @IsOptional()
  public email: string;

  @IsString()
  @IsOptional()
  public qrCode: string;

  @IsString()
  @IsOptional()
  public reviewPlatforms: Array<{ platform: string; link: string; isSelected: boolean }>;

  @IsString()
  @IsOptional()
  public isCustomForm: string;
}
