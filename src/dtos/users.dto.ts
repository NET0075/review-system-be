import { IsString, IsEmail, Length, IsOptional, IsBoolean } from 'class-validator';

export class RegisterUserDto {
  @IsString()
  @Length(1, 30)
  public fullName: string;

  @IsEmail()
  public email: string;

  @IsString()
  public password: string;

  @IsString()
  public address1: string;

  @IsString()
  public address2: string;

  @IsString()
  public pinCode: string;

  @IsString()
  public state: string;

  @IsString()
  public city: string;

  @IsString()
  public country: string;

  @IsString()
  public phoneNo: string;

  @IsString()
  public qrCode: string;

  @IsString()
  public latitude: string;

  @IsString()
  public longitude: string;

  @IsBoolean()
  public isCustomForm: boolean;

  @IsBoolean()
  public isProfileUpdated: boolean;

  @IsString()
  @IsOptional()
  public reviewPlatformLinks: Array<{ platform: string; link: string; isSelected: boolean }>;
}

export class LoginUserDto {
  @IsEmail()
  public email: string;

  @IsString()
  @IsOptional()
  public password: string;
}

export class ForgotPasswordDTO {
  @IsEmail()
  public email: string;
}

export class ResetPasswordDTO {
  @IsString()
  public token: string;
  @IsString()
  public newPassword: string;
  // @IsString()
  // public confirmPassword: string;
}

export class RegisterStaffDto {
  @IsString()
  @Length(1, 30)
  public fullName: string;

  @IsEmail()
  public email: string;

  @IsString()
  public password: string;

  @IsString()
  public roleId: string;
}

export class UpdateStaffDto {
  @IsString()
  @IsOptional()
  @Length(1, 30)
  public fullName: string;

  @IsEmail()
  @IsOptional()
  public email: string;

  @IsString()
  @IsOptional()
  public password: string;

  @IsString()
  @IsOptional()
  public roleId: string;
}
