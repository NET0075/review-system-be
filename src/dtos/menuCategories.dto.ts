import { IsBoolean, IsEnum, IsNumber, IsOptional, IsString, Max, Min } from 'class-validator';

export class MenuCategories {
  @IsString()
  public catName: string;

  @IsString()
  public userId: string;
}

export class Categories {
  @IsString()
  @IsOptional()
  public catName: string;

  @IsString()
  @IsOptional()
  public userId: string;
}
