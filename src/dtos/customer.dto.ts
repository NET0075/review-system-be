import { IsAlphanumeric, IsEmail, IsString, Length, Matches } from 'class-validator';

export class LoginCustomerDTO {
  @IsString()
  @Matches(/^(?:\+|0{0,2})\d{1,3}[-.\s]?\(?(?:\d{1,4})\)?[-.\s]?\d{3}[-.\s]?\d{4}$/, { message: 'Enter valid mobile number' })
  public mobileNumber: string;
}

export class RegisterCustomerDto {
  @IsString()
  @Length(1, 30)
  public firstName: string;

  @IsString()
  @Length(1, 30)
  public lastName: string;

  @IsEmail()
  public email: string;

  @IsAlphanumeric()
  public number: string;
}
