import { IsArray, IsEnum, IsNumber, IsOptional, IsString, Max } from 'class-validator';

export class ReviewPlatform {
  @IsString()
  public title: string;
}

export class ReviewPlatformDto {
  @IsString()
  @IsOptional()
  public title: string;

  @IsString()
  @IsOptional()
  public logo: string;
}
